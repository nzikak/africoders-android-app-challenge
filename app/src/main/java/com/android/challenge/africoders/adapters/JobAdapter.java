package com.android.challenge.africoders.adapters;


import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.challenge.africoders.R;
import com.android.challenge.africoders.activities.ViewJobActivity;
import com.android.challenge.africoders.interfaces.LoadMore;
import com.android.challenge.africoders.models.Job;
import com.android.challenge.africoders.utils.FormatTime;
import com.bumptech.glide.Glide;

import java.util.List;

public class JobAdapter extends RecyclerView.Adapter{

    private static final int LOAD_MORE_VIEW_TYPE = 4;
    private static final int JOB_VIEW_TYPE = 2;
    private List<Job> mJobList;
    private Activity mActivity;
    private boolean mIsLoading;
    private LoadMore mLoadMore;
    private String mUsername;
    private String mUserAvatarUrl;
    private String mUserAuthCode;
    private int mLastItemVisible, mTotalItemCount, mVisibleItemCount;

    public JobAdapter(Activity activity, List<Job> jobList, RecyclerView recyclerView, String username, String userAvatarUrl, String userAuthCode) {
        this.mJobList = jobList;
        this.mActivity = activity;
        this.mUsername = username;
        this.mUserAvatarUrl = userAvatarUrl;
        this.mUserAuthCode = userAuthCode;

        final LinearLayoutManager layoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerview, int dx, int dy) {
                mLastItemVisible = layoutManager.findLastVisibleItemPosition();
                mTotalItemCount = layoutManager.getItemCount();
                mVisibleItemCount = layoutManager.getChildCount();

                if(!mIsLoading && mTotalItemCount < (mLastItemVisible + mVisibleItemCount)) {
                    if(mLoadMore !=  null) {
                        mLoadMore.onLoadMore();

                        mIsLoading = true;
                    }
                }
            }
        });
    }

    private class LoadMoreViewHolder extends RecyclerView.ViewHolder {
        private ProgressBar mProgressBar;

        LoadMoreViewHolder(View itemView) {
            super(itemView);

            mProgressBar = itemView.findViewById(R.id.load_more_progress_bar);
        }
    }

    private class JobViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        private TextView mUsernameTextView, mTimePostedTextView, mTitleTextView, mLikesCountTextView, mDislikesCountTextView,
                mCommentsTextView, mViewsCountTextView;

        private ImageView mUserAvatarImageView;

        JobViewHolder(View itemView) {
            super(itemView);

            mUsernameTextView = itemView.findViewById(R.id.job_list_username);
            mTimePostedTextView = itemView.findViewById(R.id.job_list_time);
            mTitleTextView = itemView.findViewById(R.id.job_list_title);
            mLikesCountTextView = itemView.findViewById(R.id.job_list_likes_text_view);
            mDislikesCountTextView = itemView.findViewById(R.id.job_list_dislikes_text_view);
            mCommentsTextView = itemView.findViewById(R.id.job_list_comments_text_view);
            mViewsCountTextView = itemView.findViewById(R.id.job_list_views);

            mUserAvatarImageView = itemView.findViewById(R.id.job_list_user_avatar);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            Job job = mJobList.get(getAdapterPosition());
            Intent intent = new Intent(mActivity, ViewJobActivity.class);
            intent.putExtra("post_id", job.getJobPostId());
            intent.putExtra("username", mUsername);
            intent.putExtra("user_avatar_url", mUserAvatarUrl);
            intent.putExtra("user_auth_code", mUserAuthCode);
            mActivity.startActivity(intent);
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if(viewType == JOB_VIEW_TYPE) {
            View view = LayoutInflater.from(mActivity).inflate(R.layout.job_list_item, parent, false);

            return new JobViewHolder(view);
        }
        else if(viewType == LOAD_MORE_VIEW_TYPE) {
            View view = LayoutInflater.from(mActivity).inflate(R.layout.loading_items_progress_bar, parent, false);

            return new LoadMoreViewHolder(view);
        }

        return null;
    }

    @Override
    public int getItemViewType(int position) {
        return mJobList.get(position) == null ? LOAD_MORE_VIEW_TYPE : JOB_VIEW_TYPE;
    }

    @Override
    public int getItemCount() {
        return mJobList.size();
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if(holder instanceof LoadMoreViewHolder) {
            LoadMoreViewHolder viewHolder = (LoadMoreViewHolder) holder;
            viewHolder.mProgressBar.setIndeterminate(true);
        }

        else if(holder instanceof JobViewHolder) {
            Job job = mJobList.get(position);
            JobViewHolder viewHolder = (JobViewHolder) holder;
            //noinspection deprecation
            String jobTitle = Html.fromHtml(job.getJobTitle()).toString();
            jobTitle = jobTitle.trim();
            String timePosted = FormatTime.formatTime(job.getJobTimePosted());

            Glide.with(mActivity).load(job.getJobUserAvatarUrl()).into(viewHolder.mUserAvatarImageView);

            viewHolder.mUsernameTextView.setText(job.getJobUsername());
            viewHolder.mTimePostedTextView.setText(timePosted);
            viewHolder.mTitleTextView.setText(jobTitle);
            viewHolder.mViewsCountTextView.setText(job.getJobViewsCount() + " Views");
            viewHolder.mLikesCountTextView.setText(job.getJobLikesCount());
            viewHolder.mDislikesCountTextView.setText(job.getJobDislikesCount());
            viewHolder.mCommentsTextView.setText(job.getJobCommentCount());
        }
    }

    public void setLoadMore(LoadMore loadMore) {
        mLoadMore = loadMore;
    }

    public void setLoaded() {
        mIsLoading = false;
    }
}
