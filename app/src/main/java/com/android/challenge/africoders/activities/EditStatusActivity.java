package com.android.challenge.africoders.activities;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.challenge.africoders.NetworkRequest;
import com.android.challenge.africoders.R;
import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class EditStatusActivity extends AppCompatActivity {
    private static final String LOG_TAG = EditStatusActivity.class.getSimpleName();
    private EditText mPostContent;
    private String mUsername = "";
    private String mAuthCode = "";
    private String mTextToBeEdited = "";
    private int mPostId;
    private int mAdapterPosition;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post_status);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_close_black_24dp);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        ImageView userAvatar = findViewById(R.id.post_user_avatar);
        mPostContent = findViewById(R.id.post_input_field);

        String avatarUrl = "";

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            avatarUrl = extras.getString("user_avatar_url_key");
            mUsername = extras.getString("username_key");
            mAuthCode = extras.getString("auth_code_key");
            mTextToBeEdited = extras.getString("post_content_key");
            mPostId = extras.getInt("post_id_key");
            mAdapterPosition = extras.getInt("adapter_position_key");
        }

        mPostContent.setText(mTextToBeEdited);
        mPostContent.setSelection(mPostContent.getText().length());
        mPostContent.setFocusable(true);
        Glide.with(EditStatusActivity.this).load(avatarUrl).into(userAvatar);


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.post_menu, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.menu_post) {
            sendPost();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void sendPost() {
        //noinspection deprecation
        String content = Html.toHtml(mPostContent.getText());
        content = content.trim();

        if (!content.isEmpty()) {
            makeNetworkRequest(content, mPostId);
        } else {
            Toast.makeText(EditStatusActivity.this, "Post cannot be empty", Toast.LENGTH_SHORT).show();
        }
    }

    private void makeNetworkRequest(final String postContent, final int postId) {
        String postStatusUrl = "https://api.africoders.com/v1/edit/post";
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnectedOrConnecting()) {
            StringRequest postStatusRequest = new StringRequest(Request.Method.POST, postStatusUrl,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                JSONObject jsonResponse = new JSONObject(response);
                                String status = jsonResponse.getString("status");
                                if (status.matches("success")) {
                                    int id = jsonResponse.getInt("id");
                                    Toast.makeText(EditStatusActivity.this, "Status Edited!", Toast.LENGTH_SHORT).show();
                                    Intent intent = new Intent();
                                    intent.putExtra("edited_post_id", id);
                                    intent.putExtra("adapter_position", mAdapterPosition);
                                    setResult(RESULT_OK, intent);
                                    finish();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    NetworkResponse errorNetworkResponse = error.networkResponse;
                    if (errorNetworkResponse != null && errorNetworkResponse.data != null) {
                        String errorResponse = new String(errorNetworkResponse.data);
                        try {
                            JSONObject errorJsonResponse = new JSONObject(errorResponse);
                            JSONObject errorJson = errorJsonResponse.getJSONObject("error");
                            JSONArray errorBody = errorJson.getJSONArray("body");

                            String errorMessage = errorBody.getString(0);

                            Toast.makeText(EditStatusActivity.this, errorMessage, Toast.LENGTH_SHORT).show();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    error.printStackTrace();
                }
            }) {

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("Accept", "application/json");
                    headers.put("Authorization", mUsername + " " + mAuthCode);

                    Log.d(LOG_TAG, mUsername + " " + mAuthCode);
                    return headers;
                }

                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<>();
                    params.put("id", String.valueOf(postId));
                    params.put("body", postContent);
                    return params;
                }
            };

            NetworkRequest.getInstance().addToQueue(postStatusRequest);
        } else {
            Toast.makeText(EditStatusActivity.this, "No Internet Connection", Toast.LENGTH_SHORT).show();
        }

    }


}
