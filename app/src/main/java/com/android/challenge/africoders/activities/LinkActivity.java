package com.android.challenge.africoders.activities;

import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.challenge.africoders.NetworkRequest;
import com.android.challenge.africoders.R;
import com.android.challenge.africoders.adapters.LinkAdapter;
import com.android.challenge.africoders.interfaces.LoadMore;
import com.android.challenge.africoders.models.Link;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class LinkActivity extends AppCompatActivity {

    private List<Link> mLinkList;
    private RecyclerView mRecyclerView;
    private LinkAdapter mLinkAdapter;
    private SwipeRefreshLayout mRefreshLayout;
    private FloatingActionButton mPostButton;
    private ProgressBar mProgressBar;
    private String mUsername, mUserAvatarUrl, mUserAuthCode;
    private int mCurrentPage = 1;
    private int mTotalPages;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_link);

        Bundle userDetails = getIntent().getExtras();
        if(userDetails != null) {
            mUsername = userDetails.getString("username");
            mUserAvatarUrl = userDetails.getString("user_avatar_url");
            mUserAuthCode = userDetails.getString("user_auth_code");
        }

        mRecyclerView = findViewById(R.id.link_activity_recycler_view);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mRefreshLayout = findViewById(R.id.link_activity_swipeRefresh);
        mPostButton = findViewById(R.id.link_activity_floating_action_button);
        mProgressBar = findViewById(R.id.link_activity_progress_bar);

        getLinkItems();

        mPostButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        mRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getLinkItems();
            }
        });
    }

    private void getLinkItems() {
        String linkUrl = "https://api.africoders.com/v1/posts?category=link&order=published_at|desc&limit=20";


        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnectedOrConnecting()) {
            JsonObjectRequest linkRequest = new JsonObjectRequest(Request.Method.GET, linkUrl, null,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            mProgressBar.setVisibility(View.GONE);
                            try {
                                if(mRefreshLayout.isRefreshing()) {
                                    mLinkList.clear();
                                    mLinkAdapter.notifyDataSetChanged();
                                }
                                else {
                                    mLinkList = new ArrayList<>();
                                }
                                JSONArray dataArray = response.getJSONArray("data");
                                for (int i = 0; i < dataArray.length(); i++) {
                                    JSONObject linkDetails = dataArray.getJSONObject(i);
                                    int postId = linkDetails.getInt("id");
                                    String likesCount = String.valueOf(linkDetails.getInt("likes"));
                                    String commentCount = linkDetails.getString("replies");
                                    String dislikesCount = String.valueOf(linkDetails.getInt("dislikes"));
                                    String viewsCount = linkDetails.getString("views");
                                    String linkTitle = linkDetails.getString("title");
                                    String linkContent = linkDetails.getString("body");

                                    JSONObject timeDetails = linkDetails.getJSONObject("created");
                                    String timeCreated = timeDetails.getString("date");

                                    JSONObject userDetails = linkDetails.getJSONObject("user");
                                    String username = userDetails.getString("name");
                                    String avatarUrl = userDetails.getString("avatarUrl");


                                    mLinkList.add(new Link(avatarUrl, username, linkTitle, linkContent, timeCreated,
                                            viewsCount, commentCount, likesCount, dislikesCount, postId));

                                }

                                JSONObject metaObjectDetails = response.getJSONObject("meta");
                                JSONObject paginationObjectDetails = metaObjectDetails.getJSONObject("pagination");
                                mTotalPages = paginationObjectDetails.getInt("total_pages");

                                if(mRefreshLayout.isRefreshing()) {
                                    mLinkAdapter.notifyDataSetChanged();
                                    mRefreshLayout.setRefreshing(false);
                                    mCurrentPage = 1;
                                }
                                else {
                                    updateLinkList(mLinkList);
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    mProgressBar.setVisibility(View.GONE);
                    error.printStackTrace();
                }
            });

            NetworkRequest.getInstance().addToQueue(linkRequest);

        } else {
            mProgressBar.setVisibility(View.GONE);
            Toast.makeText(LinkActivity.this, "No Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }

    private void updateLinkList(List<Link> linkList) {
        mLinkAdapter = new LinkAdapter(LinkActivity.this, linkList, mRecyclerView, mUsername, mUserAvatarUrl, mUserAuthCode);


        mRecyclerView.setAdapter(mLinkAdapter);
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnectedOrConnecting()) {
            mLinkAdapter.setLoadMore(new LoadMore() {
                @Override
                public void onLoadMore() {
                    if (mCurrentPage <= mTotalPages) {
                        mLinkList.add(null);
                        mLinkAdapter.notifyItemInserted(mLinkList.size() - 1);
                        mCurrentPage++;
                        loadMoreLinkPost(mCurrentPage);
                    } else {
                        Toast.makeText(LinkActivity.this, "No more links", Toast.LENGTH_SHORT).show();
                    }
                }
            });
        } else {
            mCurrentPage--;
            Toast.makeText(LinkActivity.this, "No Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }

    private void loadMoreLinkPost(int pageOffset) {

        String statusUrl = "https://api.africoders.com/v1/posts?category=link&order=published_at|desc&limit=20&page=" + pageOffset;

        JsonObjectRequest statusRequest = new JsonObjectRequest(Request.Method.GET, statusUrl, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            mLinkList.remove(mLinkList.size() - 1);
                            mLinkAdapter.notifyItemRemoved(mLinkList.size() - 1);

                            JSONArray dataArray = response.getJSONArray("data");
                            for (int i = 0; i < dataArray.length(); i++) {
                                JSONObject linkDetails = dataArray.getJSONObject(i);
                                int postId = linkDetails.getInt("id");
                                String likesCount = String.valueOf(linkDetails.getInt("likes"));
                                String commentCount = linkDetails.getString("replies");
                                String dislikesCount = String.valueOf(linkDetails.getInt("dislikes"));
                                String viewsCount = linkDetails.getString("views");
                                String linkTitle = linkDetails.getString("title");
                                String linkContent = linkDetails.getString("body");

                                JSONObject timeDetails = linkDetails.getJSONObject("created");
                                String timeCreated = timeDetails.getString("date");

                                JSONObject userDetails = linkDetails.getJSONObject("user");
                                String username = userDetails.getString("name");
                                String avatarUrl = userDetails.getString("avatarUrl");


                                mLinkList.add(new Link(avatarUrl, username, linkTitle, linkContent, timeCreated,
                                        viewsCount,commentCount, likesCount, dislikesCount, postId));

                            }

                            mRefreshLayout.setRefreshing(false);
                            mLinkAdapter.notifyDataSetChanged();
                            mLinkAdapter.setLoaded();


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                mProgressBar.setVisibility(View.GONE);
                mCurrentPage--;
                error.printStackTrace();
            }
        });

        NetworkRequest.getInstance().addToQueue(statusRequest);

    }
}
