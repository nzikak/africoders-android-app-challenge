package com.android.challenge.africoders.adapters;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.challenge.africoders.R;
import com.android.challenge.africoders.models.JobComment;
import com.android.challenge.africoders.utils.FormatTime;
import com.bumptech.glide.Glide;

import java.util.List;

public class JobCommentAdapter extends RecyclerView.Adapter<JobCommentAdapter.JobCommentViewHolder> {
    private Context mContext;
    private List<JobComment> mCommentList;

    public JobCommentAdapter(Context context, List<JobComment> commentList) {
        mContext = context;
        mCommentList = commentList;
    }

    @Override
    public JobCommentAdapter.JobCommentViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.job_comments_list_item, parent, false);

        return new JobCommentViewHolder(view);
    }

    @Override
    public void onBindViewHolder(JobCommentAdapter.JobCommentViewHolder viewHolder, int position) {
        JobComment jobComment = mCommentList.get(position);
        //noinspection deprecation
        String commentBody = Html.fromHtml(jobComment.getCommentBody()).toString();
        commentBody = commentBody.trim();

        Glide.with(mContext).load(jobComment.getUserAvatarUrl()).into(viewHolder.mUserAvatar);

        viewHolder.mUsername.setText(jobComment.getCommentUsername());
        viewHolder.mTimePosted.setText(FormatTime.formatTime(jobComment.getTimePosted()));
        viewHolder.mCommentBody.setText(commentBody);
        viewHolder.mCommentLikes.setText(jobComment.getCommentLikesCount());
        viewHolder.mCommentDislikes.setText(jobComment.getCommentDislikesCount());


    }

    @Override
    public int getItemCount() {
        return mCommentList.size();
    }

    class JobCommentViewHolder extends RecyclerView.ViewHolder {
        private ImageView mUserAvatar;
        private TextView mUsername, mTimePosted, mCommentBody, mCommentLikes, mCommentDislikes;

        JobCommentViewHolder(View itemView) {
            super(itemView);

            mUserAvatar = itemView.findViewById(R.id.job_comment_user_avatar);
            mUsername = itemView.findViewById(R.id.job_comment_username);
            mTimePosted = itemView.findViewById(R.id.job_comment_time_posted);
            mCommentBody = itemView.findViewById(R.id.job_comment_body);
            mCommentLikes = itemView.findViewById(R.id.job_comment_likes);
            mCommentDislikes = itemView.findViewById(R.id.job_comment_dislikes);
        }

    }

}
