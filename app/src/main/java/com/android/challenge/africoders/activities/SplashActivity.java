package com.android.challenge.africoders.activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;

import com.android.challenge.africoders.R;
import com.android.challenge.africoders.utils.SaveSharedPreference;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.Splash_NoActionBar);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (SaveSharedPreference.getLoggedInStatus(SplashActivity.this)) {
                    startActivity(new Intent(SplashActivity.this, StatusActivity.class));
                    finish();
                } else {
                    startActivity(new Intent(SplashActivity.this, StartActivity.class));
                    finish();
                }
            }
        }, 2000);
    }
}
