package com.android.challenge.africoders.adapters;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.challenge.africoders.R;
import com.android.challenge.africoders.models.BlogComment;
import com.android.challenge.africoders.utils.FormatTime;
import com.bumptech.glide.Glide;


import java.util.List;

public class BlogCommentAdapter extends RecyclerView.Adapter<BlogCommentAdapter.BlogCommentViewHolder> {
    private Context mContext;
    private List<BlogComment> mCommentList;

    public BlogCommentAdapter(Context context, List<BlogComment> commentList) {
        mContext = context;
        mCommentList = commentList;
    }

    @Override
    public BlogCommentAdapter.BlogCommentViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.blog_comments_list_item, parent, false);

        return new BlogCommentViewHolder(view);
    }

    @Override
    public void onBindViewHolder(BlogCommentAdapter.BlogCommentViewHolder viewHolder, int position) {
        BlogComment blogComment = mCommentList.get(position);
        //noinspection deprecation
        String commentBody = Html.fromHtml(blogComment.getCommentBody()).toString();
        commentBody = commentBody.trim();

        Glide.with(mContext).load(blogComment.getUserAvatarUrl()).into(viewHolder.mUserAvatar);

        viewHolder.mUsername.setText(blogComment.getCommentUsername());
        viewHolder.mTimePosted.setText(FormatTime.formatTime(blogComment.getTimePosted()));
        viewHolder.mCommentBody.setText(commentBody);
        viewHolder.mCommentLikes.setText(blogComment.getCommentLikesCount());
        viewHolder.mCommentDislikes.setText(blogComment.getCommentDislikesCount());


    }

    @Override
    public int getItemCount() {
        return mCommentList.size();
    }

    class BlogCommentViewHolder extends RecyclerView.ViewHolder {
        private ImageView mUserAvatar;
        private TextView mUsername, mTimePosted, mCommentBody, mCommentLikes, mCommentDislikes;

        BlogCommentViewHolder(View itemView) {
            super(itemView);

            mUserAvatar = itemView.findViewById(R.id.blog_comment_user_avatar);
            mUsername = itemView.findViewById(R.id.blog_comment_username);
            mTimePosted = itemView.findViewById(R.id.blog_comment_time_posted);
            mCommentBody = itemView.findViewById(R.id.blog_comment_body);
            mCommentLikes = itemView.findViewById(R.id.blog_comment_likes);
            mCommentDislikes = itemView.findViewById(R.id.blog_comment_dislikes);
        }

    }

}
