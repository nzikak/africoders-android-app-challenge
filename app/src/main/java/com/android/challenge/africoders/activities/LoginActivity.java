package com.android.challenge.africoders.activities;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.challenge.africoders.NetworkRequest;
import com.android.challenge.africoders.R;
import com.android.challenge.africoders.utils.SaveSharedPreference;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class LoginActivity extends AppCompatActivity {

    private static final String LOG_TAG = LoginActivity.class.getSimpleName();
    private EditText mUsernameField, mPasswordField;
    private TextInputLayout mUsernameFieldWrapper, mPasswordFieldWrapper;
    private boolean mUsernameError = false;
    private boolean mPasswordError = false;
    private ProgressBar mProgressBar;
    private Button mLoginButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        mUsernameField = findViewById(R.id.login_username_input_field);
        mPasswordField = findViewById(R.id.login_password_input_field);
        mUsernameFieldWrapper = findViewById(R.id.login_username_field_wrapper);
        mPasswordFieldWrapper = findViewById(R.id.login_password_field_wrapper);

        mLoginButton = findViewById(R.id.login_button);
        mProgressBar = findViewById(R.id.login_progress_bar);

        mLoginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String username = mUsernameField.getText().toString();
                String password = mPasswordField.getText().toString();

                username = username.trim();
                password = password.trim();

                checkForErrors(username, password);

                if (!mUsernameError && !mPasswordError) {
                    mProgressBar.setVisibility(View.VISIBLE);
                    mLoginButton.setVisibility(View.GONE);
                    login(username, password);
                }

            }
        });

        clearErrors();


    }

    private void checkForErrors(String username, String password) {
        if (username.isEmpty()) {
            mUsernameFieldWrapper.setError("Username Cannot Be Empty");
            mUsernameError = true;
        }

        if (password.isEmpty()) {
            mPasswordFieldWrapper.setError("Password Cannot Be Empty");
            mPasswordError = true;
        }
    }

    private void clearErrors() {
        mUsernameField.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (mUsernameError) {
                    mUsernameFieldWrapper.setError(null);
                    mUsernameError = false;
                }
                return false;
            }
        });

        mPasswordField.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (mPasswordError) {
                    mPasswordFieldWrapper.setError(null);
                    mPasswordError = false;
                }
                return false;
            }
        });
    }

    private void login(final String username, final String password) {
        String loginUrl = "https://api.africoders.com/v1/user/login";
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnectedOrConnecting()) {
            StringRequest stringJsonRequest = new StringRequest(Request.Method.POST, loginUrl,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                JSONObject jsonResponse = new JSONObject(response);
                                String status = jsonResponse.getString("status");
                                if (status.matches("success")) {
                                    SaveSharedPreference.setUserDetails(LoginActivity.this, username, password);
                                    SaveSharedPreference.setLoggedInStatus(LoginActivity.this, true);
                                    startActivity(new Intent(LoginActivity.this, StatusActivity.class));
                                    if (StartActivity.sStartActivity != null) {
                                        StartActivity.sStartActivity.finish();
                                    }
                                    finish();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    mProgressBar.setVisibility(View.GONE);
                    mLoginButton.setVisibility(View.VISIBLE);

                    NetworkResponse networkResponse = error.networkResponse;

                    if (networkResponse != null && networkResponse.data != null) {
                        String jsonError = new String(networkResponse.data);
                        try {
                            JSONObject jsonResponse = new JSONObject(jsonError);
                            String status = jsonResponse.optString("status");

                            if (!status.matches("success")) {
                                Toast.makeText(LoginActivity.this, "Invalid Details", Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    error.printStackTrace();
                }
            }) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<>();
                    params.put("name", username);
                    params.put("password", password);

                    return params;
                }
            };

            NetworkRequest.getInstance().addToQueue(stringJsonRequest);
        } else {
            mProgressBar.setVisibility(View.GONE);
            mLoginButton.setVisibility(View.VISIBLE);
            Toast.makeText(LoginActivity.this, "No Internet Connection", Toast.LENGTH_SHORT).show();
        }


    }
}
