package com.android.challenge.africoders.activities;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.challenge.africoders.NetworkRequest;
import com.android.challenge.africoders.R;
import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class PostStatusActivity extends AppCompatActivity {
    private static final String LOG_TAG = PostStatusActivity.class.getSimpleName();
    private EditText postContent;
    private String username = "";
    private String authCode = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post_status);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_close_black_24dp);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        ImageView userAvatar = findViewById(R.id.post_user_avatar);
        postContent = findViewById(R.id.post_input_field);

        String avatarUrl = "";

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            avatarUrl = extras.getString("avatar_url_key");
            username = extras.getString("username_key");
            authCode = extras.getString("auth_code_key");
        }

        Glide.with(PostStatusActivity.this).load(avatarUrl).into(userAvatar);


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.post_menu, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.menu_post) {
            sendPost();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void sendPost() {
        //noinspection deprecation
        String content = Html.toHtml(postContent.getText());
        content = content.trim();

        if (!content.isEmpty()) {
            makeNetworkRequest(content);
        } else {
            Toast.makeText(PostStatusActivity.this, "Post cannot be empty", Toast.LENGTH_SHORT).show();
        }
    }

    private void makeNetworkRequest(final String postContent) {
        String postStatusUrl = "https://api.africoders.com/v1/post/status";
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnectedOrConnecting()) {
            StringRequest postStatusRequest = new StringRequest(Request.Method.POST, postStatusUrl,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                JSONObject jsonResponse = new JSONObject(response);
                                String status = jsonResponse.getString("status");
                                if (status.matches("success")) {
                                    int id = jsonResponse.getInt("id");
                                    Toast.makeText(PostStatusActivity.this, "Status Sent!", Toast.LENGTH_SHORT).show();
                                    Intent intent = new Intent();
                                    intent.putExtra("status_post_id", id);
                                    setResult(RESULT_OK, intent);
                                    finish();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    NetworkResponse errorNetworkResponse = error.networkResponse;
                    if (errorNetworkResponse != null && errorNetworkResponse.data != null) {
                        String errorResponse = new String(errorNetworkResponse.data);
                        try {
                            JSONObject errorJsonResponse = new JSONObject(errorResponse);
                            JSONObject errorJson = errorJsonResponse.getJSONObject("error");
                            JSONArray errorBody = errorJson.getJSONArray("body");

                            String errorMessage = errorBody.getString(0);

                            Toast.makeText(PostStatusActivity.this, errorMessage, Toast.LENGTH_SHORT).show();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    error.printStackTrace();
                }
            }) {

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("Accept", "application/json");
                    headers.put("Authorization", username + " " + authCode);

                    Log.d(LOG_TAG, username + " " + authCode);
                    return headers;
                }

                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<>();
                    params.put("body", postContent);
                    return params;
                }
            };

            NetworkRequest.getInstance().addToQueue(postStatusRequest);
        } else {
            Toast.makeText(PostStatusActivity.this, "No Internet Connection", Toast.LENGTH_SHORT).show();
        }

    }


}
