package com.android.challenge.africoders.interfaces;


public interface LoadMore {
    void onLoadMore();
}
