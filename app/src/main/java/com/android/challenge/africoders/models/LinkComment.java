package com.android.challenge.africoders.models;


public class LinkComment {

    private String mUserAvatarUrl, mUsername, mTimePosted, mCommentBody, mCommentLikesCount, mCommentDislikesCount;

    public LinkComment(String userAvatarUrl, String username, String timePosted, String commentBody,
                       String commentLikesCount, String commentDislikesCount) {

        this.mUserAvatarUrl = userAvatarUrl;
        this.mUsername = username;
        this.mTimePosted = timePosted;
        this.mCommentBody = commentBody;
        this.mCommentLikesCount = commentLikesCount;
        this.mCommentDislikesCount = commentDislikesCount;
    }

    public String getUserAvatarUrl() {
        return mUserAvatarUrl;
    }

    public String getCommentUsername() {
        return mUsername;
    }

    public String getTimePosted() {
        return mTimePosted;
    }

    public String getCommentBody() {
        return mCommentBody;
    }

    public String getCommentLikesCount() {
        return mCommentLikesCount;
    }

    public String getCommentDislikesCount() {
        return mCommentDislikesCount;
    }
}
