package com.android.challenge.africoders.adapters;


import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.challenge.africoders.R;
import com.android.challenge.africoders.activities.ViewBlogActivity;
import com.android.challenge.africoders.interfaces.LoadMore;
import com.android.challenge.africoders.models.Blog;
import com.android.challenge.africoders.utils.FormatTime;
import com.bumptech.glide.Glide;

import java.util.List;

public class BlogAdapter extends RecyclerView.Adapter{

    private static final int LOAD_MORE_VIEW_TYPE = 4;
    private static final int BLOG_VIEW_TYPE = 2;
    private List<Blog> mBlogList;
    private Activity mActivity;
    private boolean mIsLoading;
    private LoadMore mLoadMore;
    private String mUsername;
    private String mUserAvatarUrl;
    private String mUserAuthCode;
    private int mLastItemVisible, mTotalItemCount, mVisibleItemCount;

    public BlogAdapter(Activity activity, List<Blog> blogList, RecyclerView recyclerView, String username, String userAvatarUrl, String userAuthCode) {
        this.mBlogList = blogList;
        this.mActivity = activity;
        this.mUsername = username;
        this.mUserAvatarUrl = userAvatarUrl;
        this.mUserAuthCode = userAuthCode;

        final LinearLayoutManager layoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerview, int dx, int dy) {
                mLastItemVisible = layoutManager.findLastVisibleItemPosition();
                mTotalItemCount = layoutManager.getItemCount();
                mVisibleItemCount = layoutManager.getChildCount();

                if(!mIsLoading && mTotalItemCount < (mLastItemVisible + mVisibleItemCount)) {
                    if(mLoadMore !=  null) {
                        mLoadMore.onLoadMore();

                        mIsLoading = true;
                    }
                }
            }
        });
    }

    private class LoadMoreViewHolder extends RecyclerView.ViewHolder {
        private ProgressBar mProgressBar;

        LoadMoreViewHolder(View itemView) {
            super(itemView);

            mProgressBar = itemView.findViewById(R.id.load_more_progress_bar);
        }
    }

    private class BlogViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        private TextView mUsernameTextView, mTimePostedTextView, mTitleTextView, mLikesCountTextView, mDislikesCountTextView,
        mCommentsTextView, mViewsCountTextView;

        private ImageView mUserAvatarImageView;

        BlogViewHolder(View itemView) {
            super(itemView);

            mUsernameTextView = itemView.findViewById(R.id.blog_list_username);
            mTimePostedTextView = itemView.findViewById(R.id.blog_list_time);
            mTitleTextView = itemView.findViewById(R.id.blog_list_title);
            mLikesCountTextView = itemView.findViewById(R.id.blog_list_likes_text_view);
            mDislikesCountTextView = itemView.findViewById(R.id.blog_list_dislikes_text_view);
            mCommentsTextView = itemView.findViewById(R.id.blog_list_comments_text_view);
            mViewsCountTextView = itemView.findViewById(R.id.blog_list_views);

            mUserAvatarImageView = itemView.findViewById(R.id.blog_list_user_avatar);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            Blog blog = mBlogList.get(getAdapterPosition());
            Intent intent = new Intent(mActivity, ViewBlogActivity.class);
            intent.putExtra("post_id", blog.getBlogPostId());
            intent.putExtra("username", mUsername);
            intent.putExtra("user_avatar_url", mUserAvatarUrl);
            intent.putExtra("user_auth_code", mUserAuthCode);
            mActivity.startActivity(intent);
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if(viewType == BLOG_VIEW_TYPE) {
            View view = LayoutInflater.from(mActivity).inflate(R.layout.blog_list_item, parent, false);

            return new BlogViewHolder(view);
        }
        else if(viewType == LOAD_MORE_VIEW_TYPE) {
            View view = LayoutInflater.from(mActivity).inflate(R.layout.loading_items_progress_bar, parent, false);

            return new LoadMoreViewHolder(view);
        }

        return null;
    }

    @Override
    public int getItemViewType(int position) {
        return mBlogList.get(position) == null ? LOAD_MORE_VIEW_TYPE : BLOG_VIEW_TYPE;
    }

    @Override
    public int getItemCount() {
        return mBlogList.size();
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if(holder instanceof LoadMoreViewHolder) {
            LoadMoreViewHolder viewHolder = (LoadMoreViewHolder) holder;
            viewHolder.mProgressBar.setIndeterminate(true);
        }

        else if(holder instanceof BlogViewHolder) {
            Blog blog = mBlogList.get(position);
            BlogViewHolder viewHolder = (BlogViewHolder) holder;
            //noinspection deprecation
            String blogTitle = Html.fromHtml(blog.getBlogTitle()).toString();
            blogTitle = blogTitle.trim();
            String timePosted = FormatTime.formatTime(blog.getBlogTimePosted());

            Glide.with(mActivity).load(blog.getBlogUserAvatarUrl()).into(((BlogViewHolder) holder).mUserAvatarImageView);

            viewHolder.mUsernameTextView.setText(blog.getBlogUsername());
            viewHolder.mTimePostedTextView.setText(timePosted);
            viewHolder.mTitleTextView.setText(blogTitle);
            viewHolder.mViewsCountTextView.setText(blog.getBlogViewsCount() + " Views");
            viewHolder.mLikesCountTextView.setText(blog.getBlogLikesCount());
            viewHolder.mDislikesCountTextView.setText(blog.getBlogDislikesCount());
            viewHolder.mCommentsTextView.setText(blog.getBlogCommentCount());
        }
    }

    public void setLoadMore(LoadMore loadMore) {
        mLoadMore = loadMore;
    }

    public void setLoaded() {
        mIsLoading = false;
    }
}
