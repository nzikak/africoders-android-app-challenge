package com.android.challenge.africoders.activities;

import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.challenge.africoders.NetworkRequest;
import com.android.challenge.africoders.R;
import com.android.challenge.africoders.adapters.JobAdapter;
import com.android.challenge.africoders.interfaces.LoadMore;
import com.android.challenge.africoders.models.Job;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class JobActivity extends AppCompatActivity {

    private List<Job> mJobList;
    private RecyclerView mRecyclerView;
    private JobAdapter mJobAdapter;
    private SwipeRefreshLayout mRefreshLayout;
    private FloatingActionButton mPostButton;
    private ProgressBar mProgressBar;
    private String mUsername, mUserAvatarUrl, mUserAuthCode;
    private int mCurrentPage = 1;
    private int mTotalPages;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_job);

        Bundle userDetails = getIntent().getExtras();
        if(userDetails != null) {
            mUsername = userDetails.getString("username");
            mUserAvatarUrl = userDetails.getString("user_avatar_url");
            mUserAuthCode = userDetails.getString("user_auth_code");
        }

        mRecyclerView = findViewById(R.id.job_activity_recycler_view);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mRefreshLayout = findViewById(R.id.job_activity_swipeRefresh);
        mPostButton = findViewById(R.id.job_activity_floating_action_button);
        mProgressBar = findViewById(R.id.job_activity_progress_bar);

        getJobItems();

        mPostButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        mRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getJobItems();
            }
        });
    }

    private void getJobItems() {
        String jobUrl = "https://api.africoders.com/v1/posts?category=job&order=published_at|desc&limit=20";


        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnectedOrConnecting()) {
            JsonObjectRequest jobRequest = new JsonObjectRequest(Request.Method.GET, jobUrl, null,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            mProgressBar.setVisibility(View.GONE);
                            try {
                                if(mRefreshLayout.isRefreshing()) {
                                    mJobList.clear();
                                    mJobAdapter.notifyDataSetChanged();
                                }
                                else {
                                    mJobList = new ArrayList<>();
                                }
                                JSONArray dataArray = response.getJSONArray("data");
                                for (int i = 0; i < dataArray.length(); i++) {
                                    JSONObject jobDetails = dataArray.getJSONObject(i);
                                    int postId = jobDetails.getInt("id");
                                    String likesCount = String.valueOf(jobDetails.getInt("likes"));
                                    String commentCount = jobDetails.getString("replies");
                                    String dislikesCount = String.valueOf(jobDetails.getInt("dislikes"));
                                    String viewsCount = jobDetails.getString("views");
                                    String jobTitle = jobDetails.getString("title");
                                    String jobContent = jobDetails.getString("body");

                                    JSONObject timeDetails = jobDetails.getJSONObject("created");
                                    String timeCreated = timeDetails.getString("date");

                                    JSONObject userDetails = jobDetails.getJSONObject("user");
                                    String username = userDetails.getString("name");
                                    String avatarUrl = userDetails.getString("avatarUrl");


                                    mJobList.add(new Job(avatarUrl, username, jobTitle, jobContent, timeCreated,
                                            viewsCount, commentCount, likesCount, dislikesCount, postId));

                                }

                                JSONObject metaObjectDetails = response.getJSONObject("meta");
                                JSONObject paginationObjectDetails = metaObjectDetails.getJSONObject("pagination");
                                mTotalPages = paginationObjectDetails.getInt("total_pages");

                                if(mRefreshLayout.isRefreshing()) {
                                    mJobAdapter.notifyDataSetChanged();
                                    mRefreshLayout.setRefreshing(false);
                                    mCurrentPage = 1;
                                }
                                else {
                                    updateJobList(mJobList);
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    mProgressBar.setVisibility(View.GONE);
                    error.printStackTrace();
                }
            });

            NetworkRequest.getInstance().addToQueue(jobRequest);

        } else {
            mProgressBar.setVisibility(View.GONE);
            Toast.makeText(JobActivity.this, "No Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }

    private void updateJobList(List<Job> jobList) {
        mJobAdapter = new JobAdapter(JobActivity.this, jobList, mRecyclerView, mUsername, mUserAvatarUrl, mUserAuthCode);


        mRecyclerView.setAdapter(mJobAdapter);
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnectedOrConnecting()) {
            mJobAdapter.setLoadMore(new LoadMore() {
                @Override
                public void onLoadMore() {
                    if (mCurrentPage <= mTotalPages) {
                        mJobList.add(null);
                        mJobAdapter.notifyItemInserted(mJobList.size() - 1);
                        mCurrentPage++;
                        loadMoreJobPost(mCurrentPage);
                    } else {
                        Toast.makeText(JobActivity.this, "No more jobs", Toast.LENGTH_SHORT).show();
                    }
                }
            });
        } else {
            mCurrentPage--;
            Toast.makeText(JobActivity.this, "No Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }

    private void loadMoreJobPost(int pageOffset) {

        String statusUrl = "https://api.africoders.com/v1/posts?category=job&order=published_at|desc&limit=20&page=" + pageOffset;

        JsonObjectRequest statusRequest = new JsonObjectRequest(Request.Method.GET, statusUrl, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            mJobList.remove(mJobList.size() - 1);
                            mJobAdapter.notifyItemRemoved(mJobList.size() - 1);

                            JSONArray dataArray = response.getJSONArray("data");
                            for (int i = 0; i < dataArray.length(); i++) {
                                JSONObject jobDetails = dataArray.getJSONObject(i);
                                int postId = jobDetails.getInt("id");
                                String likesCount = String.valueOf(jobDetails.getInt("likes"));
                                String commentCount = jobDetails.getString("replies");
                                String dislikesCount = String.valueOf(jobDetails.getInt("dislikes"));
                                String viewsCount = jobDetails.getString("views");
                                String jobTitle = jobDetails.getString("title");
                                String jobContent = jobDetails.getString("body");

                                JSONObject timeDetails = jobDetails.getJSONObject("created");
                                String timeCreated = timeDetails.getString("date");

                                JSONObject userDetails = jobDetails.getJSONObject("user");
                                String username = userDetails.getString("name");
                                String avatarUrl = userDetails.getString("avatarUrl");


                                mJobList.add(new Job(avatarUrl, username, jobTitle, jobContent, timeCreated,
                                        viewsCount,commentCount, likesCount, dislikesCount, postId));

                            }

                            mRefreshLayout.setRefreshing(false);
                            mJobAdapter.notifyDataSetChanged();
                            mJobAdapter.setLoaded();


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                mProgressBar.setVisibility(View.GONE);
                mCurrentPage--;
                error.printStackTrace();
            }
        });

        NetworkRequest.getInstance().addToQueue(statusRequest);

    }
}
