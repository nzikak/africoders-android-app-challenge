package com.android.challenge.africoders.models;


public class Blog {
    private String mBlogUserAvatarUrl, mBlogUsername, mBlogTitle, mBlogBody, mBlogTimePosted, mBlogViewsCount,
            mBlogCommentCount, mBlogLikesCount, mBlogDislikesCount;

    private int mBlogPostId;

    public Blog(String userAvatarUrl, String username, String title, String body, String timePosted,
                String viewsCount, String commentCount, String likesCount, String dislikesCount, int postId) {

        this.mBlogUserAvatarUrl = userAvatarUrl;
        this.mBlogUsername = username;
        this.mBlogTitle = title;
        this.mBlogBody = body;
        this.mBlogTimePosted = timePosted;
        this.mBlogViewsCount = viewsCount;
        this.mBlogCommentCount = commentCount;
        this.mBlogLikesCount = likesCount;
        this.mBlogDislikesCount = dislikesCount;
        this.mBlogPostId = postId;
    }

    public void setBlogBody(String body) {
        this.mBlogBody = body;
    }

    public String getBlogUserAvatarUrl() {
        return mBlogUserAvatarUrl;
    }

    public String getBlogUsername() {
        return mBlogUsername;
    }

    public String getBlogTitle() {
        return mBlogTitle;
    }

    public String getBlogBody() {
        return mBlogBody;
    }

    public String getBlogTimePosted() {
        return mBlogTimePosted;
    }

    public String getBlogViewsCount() {
        return mBlogViewsCount;
    }

    public String getBlogCommentCount() {
        return mBlogCommentCount;
    }

    public String getBlogLikesCount() {
        return mBlogLikesCount;
    }

    public String getBlogDislikesCount() {
        return mBlogDislikesCount;
    }

    public int getBlogPostId() {
        return mBlogPostId;
    }
}
