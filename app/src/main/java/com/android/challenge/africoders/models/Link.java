package com.android.challenge.africoders.models;



public class Link {

    private String mLinkUserAvatarUrl, mLinkUsername, mLinkTitle, mLinkBody, mLinkTimePosted, mLinkViewsCount,
            mLinkCommentCount, mLinkLikesCount, mLinkDislikesCount;

    private int mLinkPostId;

    public Link(String userAvatarUrl, String username, String title, String body, String timePosted,
                String viewsCount, String commentCount, String likesCount, String dislikesCount, int postId) {

        this.mLinkUserAvatarUrl = userAvatarUrl;
        this.mLinkUsername = username;
        this.mLinkTitle = title;
        this.mLinkBody = body;
        this.mLinkTimePosted = timePosted;
        this.mLinkViewsCount = viewsCount;
        this.mLinkCommentCount = commentCount;
        this.mLinkLikesCount = likesCount;
        this.mLinkDislikesCount = dislikesCount;
        this.mLinkPostId = postId;
    }

    public void setLinkBody(String body) {
        this.mLinkBody = body;
    }

    public String getLinkUserAvatarUrl() {
        return mLinkUserAvatarUrl;
    }

    public String getLinkUsername() {
        return mLinkUsername;
    }

    public String getLinkTitle() {
        return mLinkTitle;
    }

    public String getLinkBody() {
        return mLinkBody;
    }

    public String getLinkTimePosted() {
        return mLinkTimePosted;
    }

    public String getLinkViewsCount() {
        return mLinkViewsCount;
    }

    public String getLinkCommentCount() {
        return mLinkCommentCount;
    }

    public String getLinkLikesCount() {
        return mLinkLikesCount;
    }

    public String getLinkDislikesCount() {
        return mLinkDislikesCount;
    }

    public int getLinkPostId() {
        return mLinkPostId;
    }
}
