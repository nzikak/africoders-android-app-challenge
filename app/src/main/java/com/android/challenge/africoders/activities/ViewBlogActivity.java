package com.android.challenge.africoders.activities;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.challenge.africoders.NetworkRequest;
import com.android.challenge.africoders.R;
import com.android.challenge.africoders.adapters.BlogCommentAdapter;
import com.android.challenge.africoders.models.BlogComment;
import com.android.challenge.africoders.utils.FormatTime;
import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ViewBlogActivity extends AppCompatActivity {

    private ImageView mUserAvatar, mSendCommentUserAvatar;
    private TextView mUsername, mTimePosted, mPostTitle, mPostViews, mPostBody, mPostLikes, mPostDislikes, mPostCommentCount;
    private EditText mCommentInputField;
    private ImageButton mSendCommentButton;
    private ProgressBar mProgressBar;
    private RecyclerView mRecyclerView;
    private BlogCommentAdapter mCommentAdapter;
    private List<BlogComment> mBlogCommentList;
    private String mMainUsername, mMainUserAvatarUrl, mMainUserAuthCode;
    private String mPostId = "";
    private String mPostUsername;
    private String mPostContent;
    private ImageButton mMoreButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_blog);


        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            mPostId = String.valueOf(extras.getInt("post_id"));
            mMainUsername = extras.getString("username");
            mMainUserAvatarUrl = extras.getString("user_avatar_url");
            mMainUserAuthCode = extras.getString("user_auth_code");
            fetchBlogDetails(mPostId);
        }

        mUserAvatar = findViewById(R.id.blog_post_user_avatar);
        mSendCommentUserAvatar = findViewById(R.id.blog_comment_user_avatar);
        mMoreButton = findViewById(R.id.blog_post_more_button);

        Glide.with(ViewBlogActivity.this).load(mMainUserAvatarUrl).into(mSendCommentUserAvatar);

        mRecyclerView = findViewById(R.id.blog_post_recyclerview);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerView.setNestedScrollingEnabled(false);

        mUsername = findViewById(R.id.blog_post_user_name);
        mTimePosted = findViewById(R.id.blog_post_timestamp);
        mPostViews = findViewById(R.id.blog_post_views_count);
        mPostBody = findViewById(R.id.blog_post_body);
        mPostLikes = findViewById(R.id.blog_post_likes);
        mPostDislikes = findViewById(R.id.blog_post_dislikes);
        mPostCommentCount = findViewById(R.id.blog_post_comments);
        
        mPostTitle = findViewById(R.id.blog_title_text_view);

        mCommentInputField = findViewById(R.id.blog_input_field);

        mSendCommentButton = findViewById(R.id.blog_send_comment_button);

        mProgressBar = findViewById(R.id.blog_send_comment_progress_bar);

        mSendCommentButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //noinspection deprecation
                String userComment = Html.toHtml(mCommentInputField.getText());
                userComment = userComment.trim();
                if (!userComment.isEmpty()) {
                    mSendCommentButton.setVisibility(View.GONE);
                    mProgressBar.setVisibility(View.VISIBLE);
                    postCommentRequest(mPostId, userComment);
                }
            }
        });


    }

    private void fetchBlogDetails(String postId) {
        String blogUrl = "https://api.africoders.com/v1/post?id=" + postId + "&include=comment";
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnectedOrConnecting()) {
            JsonObjectRequest getBlogRequest = new JsonObjectRequest(Request.Method.GET, blogUrl, null,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                mBlogCommentList = new ArrayList<>();
                                JSONArray dataArray = response.getJSONArray("data");
                                JSONObject blogDetails = dataArray.getJSONObject(0);


                                mPostContent = blogDetails.getString("body");
                                String viewCount = blogDetails.getString("views");
                                String blogLikes = String.valueOf(blogDetails.getInt("likes"));
                                String blogDislikes = String.valueOf(blogDetails.getInt("dislikes"));
                                String blogCommentCount = blogDetails.getString("replies");
                                String blogTitle = blogDetails.getString("title");

                                JSONObject timeDetails = blogDetails.getJSONObject("created");

                                String timePosted = timeDetails.getString("date");

                                JSONObject userDetails = blogDetails.getJSONObject("user");
                                mPostUsername = userDetails.getString("name");
                                String avatarUrl = userDetails.getString("avatarUrl");
                                
                                updateBlogPost(mPostUsername, avatarUrl, blogTitle, timePosted, mPostContent, blogLikes,
                                        blogDislikes, blogCommentCount, viewCount);

                                int commentCount = Integer.parseInt(blogCommentCount);
                                if (commentCount > 0) {
                                    JSONObject commentObject = blogDetails.getJSONObject("comment");
                                    JSONArray commentDataArray = commentObject.getJSONArray("data");
                                    for (int i = 0; i < commentDataArray.length(); i++) {
                                        JSONObject commentDetails = commentDataArray.getJSONObject(i);

                                        String commentBody = commentDetails.getString("body");
                                        String commentLikesCount = String.valueOf(commentDetails.getInt("likes"));
                                        String commentDislikesCount = String.valueOf(commentDetails.getInt("dislikes"));

                                        JSONObject commentUserDetails = commentDetails.getJSONObject("user");

                                        String commentUsername = commentUserDetails.getString("name");
                                        String commentUserAvatarUrl = commentUserDetails.getString("avatarUrl");

                                        JSONObject commentTimeDetails = commentDetails.getJSONObject("created");
                                        String commentTimePosted = commentTimeDetails.getString("date");

                                        mBlogCommentList.add(new BlogComment(commentUserAvatarUrl, commentUsername, commentTimePosted,
                                                commentBody, commentLikesCount, commentDislikesCount));
                                    }

                                    updateComment(mBlogCommentList);
                                }


                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();
                }
            });

            NetworkRequest.getInstance().addToQueue(getBlogRequest);

        } else {
            Toast.makeText(ViewBlogActivity.this, "No Internet Connection", Toast.LENGTH_SHORT).show();

        }
    }

    private void updateBlogPost(String username, String avatarUrl, String postTitle, String timePosted, String blogContent,
                              String likes, String dislikes, String commentCount, String viewCount) {

        //noinspection deprecation
        String postBody = Html.fromHtml(blogContent).toString();
        postBody = postBody.trim();

        Glide.with(ViewBlogActivity.this).load(avatarUrl).into(mUserAvatar);

        mUsername.setText(username);
        mTimePosted.setText(FormatTime.formatTime(timePosted));
        mPostBody.setText(postBody);
        mPostLikes.setText(likes);
        mPostDislikes.setText(dislikes);
        mPostCommentCount.setText(commentCount + (Integer.parseInt(commentCount) == 1 ? " Comment" : " Comments"));
        mPostViews.setText(viewCount + " Views");
        mPostTitle.setText(postTitle);

    }

    private void updateComment(List<BlogComment> blogCommentList) {
        mCommentAdapter = new BlogCommentAdapter(ViewBlogActivity.this, blogCommentList);
        mRecyclerView.setAdapter(mCommentAdapter);
    }

    private void postCommentRequest(final String postId, final String postComment) {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();

        if (networkInfo != null && networkInfo.isConnectedOrConnecting()) {
            String postCommentUrl = "https://api.africoders.com/v1/comment";
            StringRequest postCommentReq = new StringRequest(Request.Method.POST, postCommentUrl,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                JSONObject jsonResponse = new JSONObject(response);
                                String status = jsonResponse.getString("status");

                                if (status.matches("success")) {
                                    mCommentInputField.setText("");
                                    mCommentInputField.clearFocus();
                                    mProgressBar.setVisibility(View.GONE);
                                    mSendCommentButton.setVisibility(View.VISIBLE);
                                    Toast.makeText(ViewBlogActivity.this, "Comment Posted", Toast.LENGTH_SHORT).show();
                                    getLastCommentRequest(postId);
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    NetworkResponse networkResponse = error.networkResponse;
                    if (networkResponse != null && networkResponse.data != null) {
                        String errorResponse = new String(networkResponse.data);
                        try {
                            JSONObject errorJsonResponse = new JSONObject(errorResponse);
                            JSONObject errorJsonObject = errorJsonResponse.getJSONObject("error");
                            JSONArray bodyArray = errorJsonObject.getJSONArray("body");
                            String errorMessage = bodyArray.getString(0);

                            Toast.makeText(ViewBlogActivity.this, errorMessage, Toast.LENGTH_SHORT).show();
                            mProgressBar.setVisibility(View.GONE);
                            mSendCommentButton.setVisibility(View.VISIBLE);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    error.printStackTrace();
                }
            }) {
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<>();
                    params.put("body", postComment);
                    params.put("pid", postId);
                    return params;
                }

                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> params = new HashMap<>();
                    params.put("Accept", "application/json");
                    params.put("Authorization", mMainUsername + " " + mMainUserAuthCode);
                    return params;
                }
            };

            NetworkRequest.getInstance().addToQueue(postCommentReq);

        } else {
            Toast.makeText(ViewBlogActivity.this, "No Internet Connection", Toast.LENGTH_SHORT).show();
            mProgressBar.setVisibility(View.GONE);
            mSendCommentButton.setVisibility(View.VISIBLE);


        }
    }

    private void getLastCommentRequest(String postId) {
        String getCommentUrl = "https://api.africoders.com/v1/post?id=" + postId + "&include=comment";
        JsonObjectRequest getCommentRequest = new JsonObjectRequest(Request.Method.GET, getCommentUrl, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONArray dataArray = response.getJSONArray("data");
                            JSONObject contentDetailsObject = dataArray.getJSONObject(0);
                            JSONObject commentObject = contentDetailsObject.getJSONObject("comment");
                            JSONArray commentDataArray = commentObject.getJSONArray("data");

                            JSONObject lastCommentDetails = commentDataArray.getJSONObject(commentDataArray.length() - 1);

                            String commentBody = lastCommentDetails.getString("body");
                            String commentLikes = String.valueOf(lastCommentDetails.getInt("likes"));
                            String commentDislikes = String.valueOf(lastCommentDetails.getInt("dislikes"));

                            JSONObject commentTimeDetails = lastCommentDetails.getJSONObject("created");
                            String commentTimePosted = commentTimeDetails.getString("date");

                            JSONObject commentUserDetails = lastCommentDetails.getJSONObject("user");
                            String commentUsername = commentUserDetails.getString("name");
                            String commentUserAvatarUrl = commentUserDetails.getString("avatarUrl");

                            BlogComment lastComment = new BlogComment(commentUserAvatarUrl, commentUsername, commentTimePosted,
                                    commentBody, commentLikes, commentDislikes);

                            insertNewComment(lastComment);

                            String newCommentCount = contentDetailsObject.getString("replies");
                            mPostCommentCount.setText(newCommentCount + (Integer.parseInt(newCommentCount) == 1 ? " Comment" : " Comments"));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }

        });

        NetworkRequest.getInstance().addToQueue(getCommentRequest);
    }

    private void insertNewComment(BlogComment newComment) {
        int position = mBlogCommentList.size();
        if (mBlogCommentList.size() == 0) {
            mCommentAdapter = new BlogCommentAdapter(ViewBlogActivity.this, mBlogCommentList);
            mRecyclerView.setAdapter(mCommentAdapter);
        }
        mBlogCommentList.add(position, newComment);
        mCommentAdapter.notifyItemInserted(position);
        mRecyclerView.smoothScrollToPosition(position);
    }


}
