package com.android.challenge.africoders.utils;


import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class SaveSharedPreference {

    private static final String LOGGED_IN_PREF_KEY = "logged_in_status";
    private static final String USERNAME_PREF_KEY = "username_key";
    private static final String PASSWORD_PREF_KEY = "pass_key";


    public static SharedPreferences getPreference(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context);
    }


    public static void setLoggedInStatus(Context context, Boolean loggedIn) {
        SharedPreferences loggedInStatus = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = loggedInStatus.edit();
        editor.putBoolean(LOGGED_IN_PREF_KEY, loggedIn);
        editor.apply();
    }

    public static boolean getLoggedInStatus(Context context) {
        return getPreference(context).getBoolean(LOGGED_IN_PREF_KEY, false);
    }

    public static void setUserDetails(Context context, String username, String password) {
        SharedPreferences userDetails = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = userDetails.edit();
        editor.putString(USERNAME_PREF_KEY, username);
        editor.putString(PASSWORD_PREF_KEY, password);
        editor.apply();
    }

    public static String getUsername(Context context) {
        return getPreference(context).getString(USERNAME_PREF_KEY, "");
    }

    public static String getUserPassword(Context context) {
        return getPreference(context).getString(PASSWORD_PREF_KEY, "");
    }

    public static void setLoggedOut(Context context) {
        SharedPreferences userDetails = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = userDetails.edit();
        editor.remove(USERNAME_PREF_KEY);
        editor.remove(PASSWORD_PREF_KEY);
        editor.apply();
    }


}
