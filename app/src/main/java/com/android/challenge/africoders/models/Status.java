package com.android.challenge.africoders.models;


public class Status {

    private String mUserAvatarUrl, mUsername, mTimePosted, mStatusViews, mStatusContent,
            mStatusLikes, mStatusDislikes, mStatusComments;

    private int mStatusId;

    public Status(String userAvatarUrl, String username, String timePosted, String statusViews,
                  String statusContent, String statusLikes, String statusDislikes, String statusComments, int statusId) {

        this.mUserAvatarUrl = userAvatarUrl;
        this.mUsername = username;
        this.mTimePosted = timePosted;
        this.mStatusViews = statusViews;
        this.mStatusContent = statusContent;
        this.mStatusLikes = statusLikes;
        this.mStatusDislikes = statusDislikes;
        this.mStatusComments = statusComments;
        this.mStatusId = statusId;
    }

    public void setStatusContent(String statusContent) {
        this.mStatusContent = statusContent;
    }

    public String getUserAvatarUrl() {
        return mUserAvatarUrl;
    }

    public String getUsername() {
        return mUsername;
    }

    public String getTimePosted() {
        return mTimePosted;
    }

    public String getStatusViews() {
        return mStatusViews;
    }

    public String getStatusContent() {
        return mStatusContent;
    }

    public String getStatusLikes() {
        return mStatusLikes;
    }

    public String getStatusDislikes() {
        return mStatusDislikes;
    }

    public String getStatusComments() {
        return mStatusComments;
    }

    public int getStatusId() {
        return mStatusId;
    }
}
