package com.android.challenge.africoders.activities;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.challenge.africoders.NetworkRequest;
import com.android.challenge.africoders.R;
import com.android.challenge.africoders.adapters.StatusCommentAdapter;
import com.android.challenge.africoders.models.StatusComment;
import com.android.challenge.africoders.utils.FormatTime;
import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class ViewStatusActivity extends AppCompatActivity {
    private static final String LOG_TAG = ViewStatusActivity.class.getSimpleName();
    private static final int EDIT_STATUS_REQUEST_CODE = 300;
    private ImageView mUserAvatar, mSendCommentUserAvatar;
    private TextView mUsername, mTimePosted, mPostViews, mPostBody, mPostLikes, mPostDislikes, mPostCommentCount;
    private EditText mCommentInputField;
    private ImageButton mSendCommentButton;
    private ProgressBar mProgressBar;
    private RecyclerView mRecyclerView;
    private StatusCommentAdapter mCommentAdapter;
    private List<StatusComment> mStatusCommentList;
    private String mMainUsername, mMainUserAvatarUrl, mMainUserAuthCode;
    private String mPostId = "";
    private String mPostUsername;
    private String mPostContent;
    private ImageButton mMoreButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_status);


        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            mPostId = String.valueOf(extras.getInt("post_id"));
            mMainUsername = extras.getString("username");
            mMainUserAvatarUrl = extras.getString("user_avatar_url");
            mMainUserAuthCode = extras.getString("user_auth_code");
            fetchStatusDetails(mPostId);
        }

        mUserAvatar = findViewById(R.id.view_post_user_avatar);
        mSendCommentUserAvatar = findViewById(R.id.post_comment_user_avatar);
        mMoreButton = findViewById(R.id.view_post_more_button);

        Glide.with(ViewStatusActivity.this).load(mMainUserAvatarUrl).into(mSendCommentUserAvatar);

        mRecyclerView = findViewById(R.id.view_post_recyclerview);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerView.setNestedScrollingEnabled(false);

        mUsername = findViewById(R.id.view_post_user_name);
        mTimePosted = findViewById(R.id.view_post_timestamp);
        mPostViews = findViewById(R.id.view_post_views_count);
        mPostBody = findViewById(R.id.view_post_body);
        mPostLikes = findViewById(R.id.view_post_likes);
        mPostDislikes = findViewById(R.id.view_post_dislikes);
        mPostCommentCount = findViewById(R.id.view_post_comments);

        mCommentInputField = findViewById(R.id.post_comment_input_field);

        mSendCommentButton = findViewById(R.id.send_comment_button);

        mProgressBar = findViewById(R.id.send_comment_progress_bar);

        mSendCommentButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //noinspection deprecation
                String userComment = Html.toHtml(mCommentInputField.getText());
                userComment = userComment.trim();
                if (!userComment.isEmpty()) {
                    mSendCommentButton.setVisibility(View.GONE);
                    mProgressBar.setVisibility(View.VISIBLE);
                    postCommentRequest(mPostId, userComment);
                }
            }
        });

        mMoreButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                PopupMenu popupMenu = new PopupMenu(ViewStatusActivity.this, view);
                popupMenu.getMenuInflater().inflate(R.menu.popup_menu, popupMenu.getMenu());

                Menu popMenu = popupMenu.getMenu();
                if(!mMainUsername.matches(mPostUsername)) {
                    popMenu.findItem(R.id.popup_menu_edit).setVisible(false);
                    popMenu.findItem(R.id.popup_menu_delete).setVisible(false);
                    return;
                }

                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch(item.getItemId()) {
                            case R.id.popup_menu_edit:
                                //noinspection deprecation
                                String postContent = Html.fromHtml(mPostContent).toString();
                                postContent = postContent.trim();
                                Intent editStatusIntent = new Intent(ViewStatusActivity.this, EditStatusActivity.class);
                                editStatusIntent.putExtra("username_key", mMainUsername);
                                editStatusIntent.putExtra("user_avatar_url_key", mMainUserAvatarUrl);
                                editStatusIntent.putExtra("post_content_key", postContent);
                                editStatusIntent.putExtra("auth_code_key", mMainUserAuthCode);
                                editStatusIntent.putExtra("post_id_key", Integer.parseInt(mPostId));

                                startActivityForResult(editStatusIntent, EDIT_STATUS_REQUEST_CODE);
                                return true;
                            case R.id.popup_menu_delete:
                                AlertDialog.Builder builder = new AlertDialog.Builder(ViewStatusActivity.this);
                                builder.setCancelable(true);
                                builder.setTitle("Delete Status?");
                                builder.setMessage("Are you sure you want to delete status?");

                                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int i) {
                                        dialog.cancel();
                                    }
                                });

                                builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int i) {
                                        deleteStatus(mMainUsername, mMainUserAuthCode, mPostId);
                                    }
                                });

                                AlertDialog alert = builder.create();
                                alert.show();

                                return true;
                            default:
                                return false;
                        }
                    }
                });

                popupMenu.show();
            }
        });


    }

    private void fetchStatusDetails(String postId) {
        String statusUrl = "https://api.africoders.com/v1/post?id=" + postId + "&include=comment";
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnectedOrConnecting()) {
            JsonObjectRequest getStatusRequest = new JsonObjectRequest(Request.Method.GET, statusUrl, null,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                mStatusCommentList = new ArrayList<>();
                                JSONArray dataArray = response.getJSONArray("data");
                                JSONObject statusDetails = dataArray.getJSONObject(0);


                                mPostContent = statusDetails.getString("body");
                                String viewCount = statusDetails.getString("views");
                                String statusLikes = String.valueOf(statusDetails.getInt("likes"));
                                String statusDislikes = String.valueOf(statusDetails.getInt("dislikes"));
                                String statusCommentCount = statusDetails.getString("replies");

                                JSONObject timeDetails = statusDetails.getJSONObject("created");

                                String timePosted = timeDetails.getString("date");

                                JSONObject userDetails = statusDetails.getJSONObject("user");
                                mPostUsername = userDetails.getString("name");
                                String avatarUrl = userDetails.getString("avatarUrl");
                                updateStatus(mPostUsername, avatarUrl, timePosted, mPostContent, statusLikes,
                                        statusDislikes, statusCommentCount, viewCount);

                                int commentCount = Integer.parseInt(statusCommentCount);
                                if (commentCount > 0) {
                                    JSONObject commentObject = statusDetails.getJSONObject("comment");
                                    JSONArray commentDataArray = commentObject.getJSONArray("data");
                                    for (int i = 0; i < commentDataArray.length(); i++) {
                                        JSONObject commentDetails = commentDataArray.getJSONObject(i);

                                        String commentBody = commentDetails.getString("body");
                                        String commentLikesCount = String.valueOf(commentDetails.getInt("likes"));
                                        String commentDislikesCount = String.valueOf(commentDetails.getInt("dislikes"));

                                        JSONObject commentUserDetails = commentDetails.getJSONObject("user");

                                        String commentUsername = commentUserDetails.getString("name");
                                        String commentUserAvatarUrl = commentUserDetails.getString("avatarUrl");

                                        JSONObject commentTimeDetails = commentDetails.getJSONObject("created");
                                        String commentTimePosted = commentTimeDetails.getString("date");

                                        mStatusCommentList.add(new StatusComment(commentUserAvatarUrl, commentUsername, commentTimePosted,
                                                commentBody, commentLikesCount, commentDislikesCount));
                                    }

                                    updateComment(mStatusCommentList);
                                }


                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();
                }
            });

            NetworkRequest.getInstance().addToQueue(getStatusRequest);

        } else {
            Toast.makeText(ViewStatusActivity.this, "No Internet Connection", Toast.LENGTH_SHORT).show();

        }
    }

    private void updateStatus(String username, String avatarUrl, String timePosted, String statusContent,
                              String likes, String dislikes, String commentCount, String viewCount) {

        //noinspection deprecation
        String postBody = Html.fromHtml(statusContent).toString();
        postBody = postBody.trim();

        Glide.with(ViewStatusActivity.this).load(avatarUrl).into(mUserAvatar);

        mUsername.setText(username);
        mTimePosted.setText(FormatTime.formatTime(timePosted));
        mPostBody.setText(postBody);
        mPostLikes.setText(likes);
        mPostDislikes.setText(dislikes);
        mPostCommentCount.setText(commentCount + (Integer.parseInt(commentCount) == 1 ? " Comment" : " Comments"));
        mPostViews.setText(viewCount + " Views");

    }

    private void updateComment(List<StatusComment> statusCommentList) {
        mCommentAdapter = new StatusCommentAdapter(ViewStatusActivity.this, statusCommentList);
        mRecyclerView.setAdapter(mCommentAdapter);
    }

    private void postCommentRequest(final String postId, final String postComment) {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();

        if (networkInfo != null && networkInfo.isConnectedOrConnecting()) {
            String postCommentUrl = "https://api.africoders.com/v1/comment";
            StringRequest postCommentReq = new StringRequest(Request.Method.POST, postCommentUrl,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                JSONObject jsonResponse = new JSONObject(response);
                                String status = jsonResponse.getString("status");

                                if (status.matches("success")) {
                                    mCommentInputField.setText("");
                                    mCommentInputField.clearFocus();
                                    mProgressBar.setVisibility(View.GONE);
                                    mSendCommentButton.setVisibility(View.VISIBLE);
                                    Toast.makeText(ViewStatusActivity.this, "Comment Posted", Toast.LENGTH_SHORT).show();
                                    getLastCommentRequest(postId);
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    NetworkResponse networkResponse = error.networkResponse;
                    if (networkResponse != null && networkResponse.data != null) {
                        String errorResponse = new String(networkResponse.data);
                        try {
                            JSONObject errorJsonResponse = new JSONObject(errorResponse);
                            JSONObject errorJsonObject = errorJsonResponse.getJSONObject("error");
                            JSONArray bodyArray = errorJsonObject.getJSONArray("body");
                            String errorMessage = bodyArray.getString(0);

                            Toast.makeText(ViewStatusActivity.this, errorMessage, Toast.LENGTH_SHORT).show();
                            mProgressBar.setVisibility(View.GONE);
                            mSendCommentButton.setVisibility(View.VISIBLE);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    error.printStackTrace();
                }
            }) {
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<>();
                    params.put("body", postComment);
                    params.put("pid", postId);
                    return params;
                }

                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> params = new HashMap<>();
                    params.put("Accept", "application/json");
                    params.put("Authorization", mMainUsername + " " + mMainUserAuthCode);
                    return params;
                }
            };

            NetworkRequest.getInstance().addToQueue(postCommentReq);

        } else {
            Toast.makeText(ViewStatusActivity.this, "No Internet Connection", Toast.LENGTH_SHORT).show();
            mProgressBar.setVisibility(View.GONE);
            mSendCommentButton.setVisibility(View.VISIBLE);


        }
    }

    private void getLastCommentRequest(String postId) {
        String getCommentUrl = "https://api.africoders.com/v1/post?id=" + postId + "&include=comment";
        JsonObjectRequest getCommentRequest = new JsonObjectRequest(Request.Method.GET, getCommentUrl, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONArray dataArray = response.getJSONArray("data");
                            JSONObject contentDetailsObject = dataArray.getJSONObject(0);
                            JSONObject commentObject = contentDetailsObject.getJSONObject("comment");
                            JSONArray commentDataArray = commentObject.getJSONArray("data");

                            JSONObject lastCommentDetails = commentDataArray.getJSONObject(commentDataArray.length() - 1);

                            String commentBody = lastCommentDetails.getString("body");
                            String commentLikes = String.valueOf(lastCommentDetails.getInt("likes"));
                            String commentDislikes = String.valueOf(lastCommentDetails.getInt("dislikes"));

                            JSONObject commentTimeDetails = lastCommentDetails.getJSONObject("created");
                            String commentTimePosted = commentTimeDetails.getString("date");

                            JSONObject commentUserDetails = lastCommentDetails.getJSONObject("user");
                            String commentUsername = commentUserDetails.getString("name");
                            String commentUserAvatarUrl = commentUserDetails.getString("avatarUrl");

                            StatusComment lastComment = new StatusComment(commentUserAvatarUrl, commentUsername, commentTimePosted,
                                    commentBody, commentLikes, commentDislikes);

                            insertNewComment(lastComment);

                            String newCommentCount = contentDetailsObject.getString("replies");
                            mPostCommentCount.setText(newCommentCount + (Integer.parseInt(newCommentCount) == 1 ? " Comment" : " Comments"));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }

        });

        NetworkRequest.getInstance().addToQueue(getCommentRequest);
    }

    private void insertNewComment(StatusComment newComment) {
        int position = mStatusCommentList.size();
        if (mStatusCommentList.size() == 0) {
            mCommentAdapter = new StatusCommentAdapter(ViewStatusActivity.this, mStatusCommentList);
            mRecyclerView.setAdapter(mCommentAdapter);
        }
        mStatusCommentList.add(position, newComment);
        mCommentAdapter.notifyItemInserted(position);
        mRecyclerView.smoothScrollToPosition(position);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != RESULT_OK) {
            return;
        }

        if (requestCode == EDIT_STATUS_REQUEST_CODE) {
            setEditedStatus(mPostId);
        }
    }

    private void setEditedStatus(String postId) {
        String statusUrl = "https://api.africoders.com/v1/post?id=" + postId;
        JsonObjectRequest getStatusRequest = new JsonObjectRequest(Request.Method.GET, statusUrl, null,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject jsonResponse) {
                        try {
                            JSONArray dataArray = jsonResponse.getJSONArray("data");
                            JSONObject statusProperties = dataArray.getJSONObject(0);

                            String statusContent = statusProperties.getString("body");

                            String postContent = Html.fromHtml(statusContent).toString();
                            postContent = postContent.trim();


                            mPostBody.setText(postContent);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        });

        NetworkRequest.getInstance().addToQueue(getStatusRequest);
    }

    private void deleteStatus(final String username, final String authCode, final String postId) {
        String deleteStatusUrl = "https://api.africoders.com/v1/del/post";

        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        if(networkInfo != null && networkInfo.isConnectedOrConnecting()) {
            StringRequest deleteStatusRequest = new StringRequest(Request.Method.POST, deleteStatusUrl,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                JSONObject jsonResponse = new JSONObject(response);
                                String status = jsonResponse.getString("status");
                                if(status.matches("success")) {
                                    Toast.makeText(ViewStatusActivity.this, "Status Deleted!", Toast.LENGTH_SHORT).show();
                                    finish();
                                }
                            }
                            catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();
                }
            }) {
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<>();
                    params.put("id", postId);

                    return params;
                }

                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> params = new HashMap<>();
                    params.put("Accept", "application/json");
                    params.put("Authorization", username + " " + authCode);

                    return params;
                }
            };

            NetworkRequest.getInstance().addToQueue(deleteStatusRequest);
        }

        else {
            Toast.makeText(ViewStatusActivity.this, "No Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }


}
