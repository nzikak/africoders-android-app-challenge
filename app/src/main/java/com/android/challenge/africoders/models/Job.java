package com.android.challenge.africoders.models;



public class Job {

        private String mJobUserAvatarUrl, mJobUsername, mJobTitle, mJobBody, mJobTimePosted, mJobViewsCount,
                mJobCommentCount, mJobLikesCount, mJobDislikesCount;

        private int mJobPostId;

        public Job(String userAvatarUrl, String username, String title, String body, String timePosted,
                    String viewsCount, String commentCount, String likesCount, String dislikesCount, int postId) {

            this.mJobUserAvatarUrl = userAvatarUrl;
            this.mJobUsername = username;
            this.mJobTitle = title;
            this.mJobBody = body;
            this.mJobTimePosted = timePosted;
            this.mJobViewsCount = viewsCount;
            this.mJobCommentCount = commentCount;
            this.mJobLikesCount = likesCount;
            this.mJobDislikesCount = dislikesCount;
            this.mJobPostId = postId;
        }

        public void setJobBody(String body) {
            this.mJobBody = body;
        }

        public String getJobUserAvatarUrl() {
            return mJobUserAvatarUrl;
        }

        public String getJobUsername() {
            return mJobUsername;
        }

        public String getJobTitle() {
            return mJobTitle;
        }

        public String getJobBody() {
            return mJobBody;
        }

        public String getJobTimePosted() {
            return mJobTimePosted;
        }

        public String getJobViewsCount() {
            return mJobViewsCount;
        }

        public String getJobCommentCount() {
            return mJobCommentCount;
        }

        public String getJobLikesCount() {
            return mJobLikesCount;
        }

        public String getJobDislikesCount() {
            return mJobDislikesCount;
        }

        public int getJobPostId() {
            return mJobPostId;
        }

}
