package com.android.challenge.africoders.activities;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Patterns;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.challenge.africoders.NetworkRequest;
import com.android.challenge.africoders.R;
import com.android.challenge.africoders.utils.SaveSharedPreference;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class RegisterActivity extends AppCompatActivity {

    private static final String LOG_TAG = RegisterActivity.class.getSimpleName();
    private EditText mEmailField, mUsernameField, mPasswordField, mConfirmPasswordField;
    private TextInputLayout mEmailFieldContainer, mUsernameFieldWrapper, mPasswordFieldWrapper, mConfirmPasswordFieldWrapper;
    private boolean mUsernameError = false;
    private boolean mEmailError = false;
    private boolean mPasswordError = false;
    private boolean mConfirmPasswordError = false;
    private ProgressBar mProgressBar;
    private Button mRegistrationButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        mRegistrationButton = findViewById(R.id.register_button);

        mProgressBar = findViewById(R.id.registration_progress_bar);

        mEmailField = findViewById(R.id.email_input_field);
        mUsernameField = findViewById(R.id.username_input_field);
        mPasswordField = findViewById(R.id.password_input_field);
        mConfirmPasswordField = findViewById(R.id.password_confirmation_input_field);

        mEmailFieldContainer = findViewById(R.id.email_field_wrapper);
        mUsernameFieldWrapper = findViewById(R.id.username_field_wrapper);
        mPasswordFieldWrapper = findViewById(R.id.password_field_wrapper);
        mConfirmPasswordFieldWrapper = findViewById(R.id.password_confirmation_wrapper);


        mRegistrationButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String username = mUsernameField.getText().toString();
                String email = mEmailField.getText().toString();
                String password = mPasswordField.getText().toString();
                String passwordConfirmation = mConfirmPasswordField.getText().toString();

                username = username.trim();
                email = email.trim();
                password = password.trim();
                passwordConfirmation = passwordConfirmation.trim();

                checkErrors(username, email, password, passwordConfirmation);
                if (!mUsernameError && !mEmailError && !mPasswordError && !mConfirmPasswordError) {
                    mProgressBar.setVisibility(View.VISIBLE);
                    mRegistrationButton.setVisibility(View.GONE);
                    registerUser(username, email, password);
                }

            }
        });

        clearErrors();
    }


    private void clearErrors() {
        mUsernameField.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View view, MotionEvent event) {
                if (mUsernameError) {
                    mUsernameFieldWrapper.setError(null);
                    mUsernameError = false;
                }
                return false;
            }
        });

        mEmailField.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View view, MotionEvent event) {
                if (mEmailError) {
                    mEmailFieldContainer.setError(null);
                    mEmailError = false;
                }
                return false;
            }
        });

        mPasswordField.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View view, MotionEvent event) {
                if (mPasswordError) {
                    mPasswordFieldWrapper.setError(null);
                    mPasswordError = false;
                }
                return false;
            }
        });

        mConfirmPasswordField.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View view, MotionEvent event) {
                if (mConfirmPasswordError) {
                    mConfirmPasswordFieldWrapper.setError(null);
                    mConfirmPasswordError = false;
                }
                return false;
            }
        });
    }

    private void checkErrors(String username, String email, String password, String passwordConfirmation) {


        if (username.isEmpty()) {
            mUsernameFieldWrapper.setError("Username Cannot be Empty");
            mUsernameError = true;
        } else if (username.length() > 15) {
            mUsernameFieldWrapper.setError("Username Cannot be more than 15 characters");
            mUsernameError = true;
        }

        if (email.isEmpty()) {
            mEmailFieldContainer.setError("Email Cannot be Empty");
            mEmailError = true;
        } else if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            mEmailFieldContainer.setError("Invalid Email");
            mEmailError = true;
        }

        if (password.isEmpty()) {
            mPasswordFieldWrapper.setError("Password Cannot be Empty");
            mPasswordError = true;
        } else if (password.length() < 6) {
            mPasswordFieldWrapper.setError("Password must have at least 6 characters");
            mPasswordError = true;
        }

        if (passwordConfirmation.isEmpty() || !passwordConfirmation.matches(password)) {
            mConfirmPasswordFieldWrapper.setError("Passwords do not match");
            mConfirmPasswordError = true;
        }
    }

    private void registerUser(final String username, final String email, final String password) {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        String url = "https://api.africoders.com/v1/user/signup";

        if (networkInfo != null && networkInfo.isConnectedOrConnecting()) {
            StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                JSONObject jsonResponse = new JSONObject(response);
                                String status = jsonResponse.getString("status");
                                if (status.matches("success")) {
                                    Toast.makeText(RegisterActivity.this, "Registration Successful", Toast.LENGTH_SHORT).show();
                                    SaveSharedPreference.setUserDetails(RegisterActivity.this, username, password);
                                    SaveSharedPreference.setLoggedInStatus(RegisterActivity.this, true);
                                    startActivity(new Intent(RegisterActivity.this, StatusActivity.class));
                                    if (StartActivity.sStartActivity != null) {
                                        StartActivity.sStartActivity.finish();
                                    }
                                    finish();
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    mProgressBar.setVisibility(View.GONE);
                    mRegistrationButton.setVisibility(View.VISIBLE);

                    NetworkResponse networkResponse = error.networkResponse;
                    if (networkResponse != null && networkResponse.data != null) {

                        String jsonError = new String(networkResponse.data);
                        try {
                            JSONObject errorResponse = new JSONObject(jsonError);
                            JSONObject errorMessage = errorResponse.optJSONObject("error");
                            JSONArray emailError = errorMessage.optJSONArray("email");
                            JSONArray usernameError = errorMessage.optJSONArray("name");

                            if (emailError != null) {
                                mEmailFieldContainer.setError(emailError.getString(0));
                                mEmailError = true;
                            }
                            if (usernameError != null) {
                                mUsernameFieldWrapper.setError(usernameError.getString(0));
                                mUsernameError = true;
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }


                    error.printStackTrace();

                }

            }) {

                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<>();
                    params.put("name", username);
                    params.put("email", email);
                    params.put("password", password);

                    return params;
                }

            };
            NetworkRequest.getInstance().addToQueue(stringRequest);
        } else

        {
            mProgressBar.setVisibility(View.GONE);
            mRegistrationButton.setVisibility(View.VISIBLE);
            Toast.makeText(RegisterActivity.this, "No Internet Connection", Toast.LENGTH_SHORT).show();
        }

    }
}
