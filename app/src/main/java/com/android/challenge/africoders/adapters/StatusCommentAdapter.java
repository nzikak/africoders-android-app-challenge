package com.android.challenge.africoders.adapters;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.challenge.africoders.R;
import com.android.challenge.africoders.models.StatusComment;
import com.android.challenge.africoders.utils.FormatTime;
import com.bumptech.glide.Glide;

import java.util.List;

public class StatusCommentAdapter extends RecyclerView.Adapter<StatusCommentAdapter.CommentViewHolder> {
    private Context mContext;
    private List<StatusComment> mCommentList;

    public StatusCommentAdapter(Context context, List<StatusComment> commentList) {
        mContext = context;
        mCommentList = commentList;
    }

    @Override
    public StatusCommentAdapter.CommentViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.status_comments_list_item, parent, false);

        return new CommentViewHolder(view);
    }

    @Override
    public void onBindViewHolder(StatusCommentAdapter.CommentViewHolder viewHolder, int position) {
        StatusComment statusComment = mCommentList.get(position);
        //noinspection deprecation
        String commentBody = Html.fromHtml(statusComment.getCommentBody()).toString();
        commentBody = commentBody.trim();

        Glide.with(mContext).load(statusComment.getUserAvatarUrl()).into(viewHolder.mUserAvatar);

        viewHolder.mUsername.setText(statusComment.getCommentUsername());
        viewHolder.mTimePosted.setText(FormatTime.formatTime(statusComment.getTimePosted()));
        viewHolder.mCommentBody.setText(commentBody);
        viewHolder.mCommentLikes.setText(statusComment.getCommentLikesCount());
        viewHolder.mCommentDislikes.setText(statusComment.getCommentDislikesCount());


    }

    @Override
    public int getItemCount() {
        return mCommentList.size();
    }

    class CommentViewHolder extends RecyclerView.ViewHolder {
        private ImageView mUserAvatar;
        private TextView mUsername, mTimePosted, mCommentBody, mCommentLikes, mCommentDislikes;

        CommentViewHolder(View itemView) {
            super(itemView);

            mUserAvatar = itemView.findViewById(R.id.comment_user_avatar);
            mUsername = itemView.findViewById(R.id.comment_username);
            mTimePosted = itemView.findViewById(R.id.comment_time_posted);
            mCommentBody = itemView.findViewById(R.id.comment_body);
            mCommentLikes = itemView.findViewById(R.id.comment_likes);
            mCommentDislikes = itemView.findViewById(R.id.comment_dislikes);
        }

    }

}
