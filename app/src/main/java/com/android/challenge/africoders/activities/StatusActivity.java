package com.android.challenge.africoders.activities;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.challenge.africoders.NetworkRequest;
import com.android.challenge.africoders.R;
import com.android.challenge.africoders.adapters.StatusAdapter;
import com.android.challenge.africoders.interfaces.LoadMore;
import com.android.challenge.africoders.models.Status;
import com.android.challenge.africoders.utils.SaveSharedPreference;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class StatusActivity extends AppCompatActivity {

    private static final String LOG_TAG = StatusActivity.class.getSimpleName();
    private static final int POST_STATUS_ACTIVITY_REQUEST_CODE = 100;
    public static final int EDIT_STATUS_ACTIVITY_REQUEST_CODE = 200;
    private RecyclerView mRecyclerView;
    private ActionBarDrawerToggle mToggle;
    private DrawerLayout mDrawerLayout;
    private ImageView mNavUserAvatar;
    private TextView mNavUsername;
    private FloatingActionButton mPostStatus;
    private StatusAdapter mStatusAdapter;
    private List<Status> mStatusList;
    private String mUsername, mUserAvatarUrl, mUserAuthCode;
    private TextView mNoInternetTextView;
    private ProgressBar mProgressBar;
    private int mCurrentPage = 1;
    private int mTotalPages;
    private SwipeRefreshLayout mSwipeRefreshLayout;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_status);


        mDrawerLayout = findViewById(R.id.drawer_layout);
        Toolbar toolbar = findViewById(R.id.main_activity_tool_bar);
        mRecyclerView = findViewById(R.id.status_recycler_view);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        mSwipeRefreshLayout = findViewById(R.id.swipeRefresh);

        mNoInternetTextView = findViewById(R.id.no_internet);

        mProgressBar = findViewById(R.id.main_activity_progress_bar);

        mPostStatus = findViewById(R.id.status_floating_action_button);

        NavigationView navigationView = findViewById(R.id.nav_drawer);

        toolbar.setTitle(R.string.app_name);

        //Inflating NavigationView headerLayout
        View headerLayout = navigationView.getHeaderView(0);

        mNavUserAvatar = headerLayout.findViewById(R.id.nav_user_avatar);
        mNavUsername = headerLayout.findViewById(R.id.nav_username);

        mToggle = new ActionBarDrawerToggle(this, mDrawerLayout, toolbar, R.string.open, R.string.close);
        mDrawerLayout.addDrawerListener(mToggle);
        mToggle.syncState();

        updateUserDetails();
        sendRequest();

        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                sendRequest();
            }

        });

        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem item) {
                if(item.getItemId() == R.id.nav_blog && mUsername != null) {
                    Intent intent = new Intent(StatusActivity.this, BlogActivity.class);
                    intent.putExtra("username", mUsername);
                    intent.putExtra("user_avatar_url", mUserAvatarUrl);
                    intent.putExtra("user_auth_code", mUserAuthCode);
                    startActivity(intent);
                    item.setChecked(true);
                    mDrawerLayout.closeDrawers();
                    return true;
                }
                else if(item.getItemId() == R.id.nav_jobs) {
                    Intent intent = new Intent(StatusActivity.this, JobActivity.class);
                    intent.putExtra("username", mUsername);
                    intent.putExtra("user_avatar_url", mUserAvatarUrl);
                    intent.putExtra("user_auth_code", mUserAuthCode);
                    startActivity(intent);
                    item.setChecked(true);
                    mDrawerLayout.closeDrawers();
                    return true;
                }
                else if(item.getItemId() == R.id.nav_links) {
                    Intent intent = new Intent(StatusActivity.this, LinkActivity.class);
                    intent.putExtra("username", mUsername);
                    intent.putExtra("user_avatar_url", mUserAvatarUrl);
                    intent.putExtra("user_auth_code", mUserAuthCode);
                    startActivity(intent);
                    item.setChecked(true);
                    mDrawerLayout.closeDrawers();
                    return true;
                }
                else if(item.getItemId() == R.id.nav_logout) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(StatusActivity.this);
                    builder.setCancelable(true);
                    builder.setTitle("Logout");
                    builder.setMessage("are you sure you want to logout?");

                    builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int i) {
                            dialog.dismiss();
                        }
                    });

                    builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int i) {
                            SaveSharedPreference.setLoggedInStatus(StatusActivity.this, false);
                            SaveSharedPreference.setLoggedOut(StatusActivity.this);
                            startActivity(new Intent(StatusActivity.this, StartActivity.class));
                            finish();
                        }
                    });

                    AlertDialog alertDialog = builder.create();
                    alertDialog.show();
                    item.setChecked(true);
                    mDrawerLayout.closeDrawers();
                    return true;
                }

                return true;
            }
        });
    }

    private void sendRequest() {
        String statusUrl = "https://api.africoders.com/v1/posts?category=status&order=published_at|desc&limit=20";


        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnectedOrConnecting()) {
            JsonObjectRequest statusRequest = new JsonObjectRequest(Request.Method.GET, statusUrl, null,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            mProgressBar.setVisibility(View.GONE);
                            try {
                                if(mSwipeRefreshLayout.isRefreshing()) {
                                    mStatusList.clear();
                                    mStatusAdapter.notifyDataSetChanged();
                                }
                                else {
                                    mStatusList = new ArrayList<>();
                                }
                                JSONArray dataArray = response.getJSONArray("data");
                                for (int i = 0; i < dataArray.length(); i++) {
                                    JSONObject statusDetails = dataArray.getJSONObject(i);
                                    int postId = statusDetails.getInt("id");
                                    String likesCount = String.valueOf(statusDetails.getInt("likes"));
                                    String commentCount = statusDetails.getString("replies");
                                    String dislikesCount = String.valueOf(statusDetails.getInt("dislikes"));
                                    String viewsCount = statusDetails.getString("views");
                                    String statusContent = statusDetails.getString("body");

                                    JSONObject timeDetails = statusDetails.getJSONObject("created");
                                    String timeCreated = timeDetails.getString("date");

                                    JSONObject userDetails = statusDetails.getJSONObject("user");
                                    String username = userDetails.getString("name");
                                    String avatarUrl = userDetails.getString("avatarUrl");


                                    mStatusList.add(new Status(avatarUrl, username, timeCreated,
                                            viewsCount, statusContent, likesCount, dislikesCount, commentCount, postId));

                                }

                                JSONObject metaObjectDetails = response.getJSONObject("meta");
                                JSONObject paginationObjectDetails = metaObjectDetails.getJSONObject("pagination");
                                mTotalPages = paginationObjectDetails.getInt("total_pages");

                                if(mSwipeRefreshLayout.isRefreshing()) {
                                    mStatusAdapter.notifyDataSetChanged();
                                    mSwipeRefreshLayout.setRefreshing(false);
                                    mCurrentPage = 1;
                                }
                                else {
                                    updateUi(mStatusList);
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    mProgressBar.setVisibility(View.GONE);
                    error.printStackTrace();
                }
            });

            NetworkRequest.getInstance().addToQueue(statusRequest);

        } else {
            mProgressBar.setVisibility(View.GONE);
            mNoInternetTextView.setVisibility(View.VISIBLE);
        }
    }

    private void updateUserDetails() {
        String loginUrl = "https://api.africoders.com/v1/user/login";
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnectedOrConnecting()) {
            StringRequest userDetailsRequest = new StringRequest(Request.Method.POST, loginUrl,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                JSONObject baseResponse = new JSONObject(response);
                                JSONArray userDataArray = baseResponse.getJSONArray("data");
                                mUserAuthCode = baseResponse.getString("token");

                                JSONObject userDetails = userDataArray.getJSONObject(0);
                                mUsername = userDetails.getString("name");
                                mUserAvatarUrl = userDetails.getString("avatarUrl");


                                mNavUsername.setText(mUsername);
                                Glide.with(StatusActivity.this).load(mUserAvatarUrl).into(mNavUserAvatar);

                                startPostStatusActivity(mUsername, mUserAvatarUrl, mUserAuthCode);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    error.printStackTrace();
                }
            }) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<>();
                    String username = SaveSharedPreference.getUsername(StatusActivity.this);
                    String password = SaveSharedPreference.getUserPassword(StatusActivity.this);

                    params.put("name", username);
                    params.put("password", password);

                    return params;
                }
            };


            NetworkRequest.getInstance().addToQueue(userDetailsRequest);
        } else {
            Toast.makeText(StatusActivity.this, "No Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }


    private void updateUi(List<Status> statusList) {
        mStatusAdapter = new StatusAdapter(StatusActivity.this, statusList, mRecyclerView, mUsername, mUserAvatarUrl, mUserAuthCode);


        mRecyclerView.setAdapter(mStatusAdapter);
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnectedOrConnecting()) {
            mStatusAdapter.setLoadMore(new LoadMore() {
                @Override
                public void onLoadMore() {
                    if (mCurrentPage <= mTotalPages) {
                        mStatusList.add(null);
                        mStatusAdapter.notifyItemInserted(mStatusList.size() - 1);
                        mCurrentPage++;
                        loadMoreStatus(mCurrentPage);
                    } else {
                        Toast.makeText(StatusActivity.this, "No more status", Toast.LENGTH_SHORT).show();
                    }
                }
            });
        } else {
            mCurrentPage--;
            Toast.makeText(StatusActivity.this, "No Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (mToggle.onOptionsItemSelected(item)) {
            mDrawerLayout.closeDrawer(GravityCompat.START);
        }
        return super.onOptionsItemSelected(item);
    }

    private void startPostStatusActivity(final String name, final String avatarUrl, final String authCode) {

        mPostStatus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (name != null && avatarUrl != null) {
                    Intent postActivityIntent = new Intent(StatusActivity.this, PostStatusActivity.class);
                    postActivityIntent.putExtra("avatar_url_key", avatarUrl);
                    postActivityIntent.putExtra("username_key", name);
                    postActivityIntent.putExtra("auth_code_key", authCode);
                    startActivityForResult(postActivityIntent, POST_STATUS_ACTIVITY_REQUEST_CODE);
                }
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != RESULT_OK) {
            return;
        }
        if (requestCode == POST_STATUS_ACTIVITY_REQUEST_CODE && data != null) {
            Bundle extras = data.getExtras();
            insertNewStatus(extras.getInt("status_post_id"));
        }

        if (requestCode == EDIT_STATUS_ACTIVITY_REQUEST_CODE && data != null) {
            Bundle extras = data.getExtras();
            int adapterPosition =  extras.getInt("adapter_position");
            int postId = extras.getInt("edited_post_id");


            setEditedStatus(adapterPosition, postId);
        }
    }

    private void setEditedStatus(final int adapterPosition, final int postId) {
        String statusUrl = "https://api.africoders.com/v1/post?id=" + postId;
        JsonObjectRequest getStatusRequest = new JsonObjectRequest(Request.Method.GET, statusUrl, null,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject jsonResponse) {
                        try {
                            JSONArray dataArray = jsonResponse.getJSONArray("data");
                            JSONObject statusProperties = dataArray.getJSONObject(0);


                            String statusContent = statusProperties.getString("body");


                            mStatusList.get(adapterPosition).setStatusContent(statusContent);
                            mStatusAdapter.notifyItemChanged(adapterPosition);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        });

        NetworkRequest.getInstance().addToQueue(getStatusRequest);
    }

    private void insertNewStatus(final int postId) {
        String statusUrl = "https://api.africoders.com/v1/post?id=" + postId;
        JsonObjectRequest getStatusRequest = new JsonObjectRequest(Request.Method.GET, statusUrl, null,
                new Response.Listener<JSONObject>() {
                    Status newStatus = null;

                    @Override
                    public void onResponse(JSONObject jsonResponse) {
                        try {
                            JSONArray dataArray = jsonResponse.getJSONArray("data");
                            JSONObject statusProperties = dataArray.getJSONObject(0);


                            String statusContent = statusProperties.getString("body");
                            String statusDislikes = String.valueOf(statusProperties.getInt("dislikes"));
                            String statusLikes = String.valueOf(statusProperties.getInt("likes"));
                            String statusViews = statusProperties.getString("views");
                            String statusCommentsCount = statusProperties.getString("replies");

                            JSONObject timeDetails = statusProperties.getJSONObject("created");
                            String timeStatusPosted = timeDetails.getString("date");

                            JSONObject userDetails = statusProperties.getJSONObject("user");
                            String username = userDetails.getString("name");
                            String avatarUrl = userDetails.getString("avatarUrl");

                            newStatus = new Status(avatarUrl, username, timeStatusPosted, statusViews, statusContent, statusLikes,
                                    statusDislikes, statusCommentsCount, postId);

                            mStatusList.add(0, newStatus);
                            mStatusAdapter.notifyItemInserted(0);
                            mRecyclerView.smoothScrollToPosition(0);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        });

        NetworkRequest.getInstance().addToQueue(getStatusRequest);

    }

    private void loadMoreStatus(int pageOffset) {

        String statusUrl = "https://api.africoders.com/v1/posts?category=status&order=published_at|desc&limit=20&page=" + pageOffset;

        JsonObjectRequest statusRequest = new JsonObjectRequest(Request.Method.GET, statusUrl, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            mStatusList.remove(mStatusList.size() - 1);
                            mStatusAdapter.notifyItemRemoved(mStatusList.size() - 1);

                            JSONArray dataArray = response.getJSONArray("data");
                            for (int i = 0; i < dataArray.length(); i++) {
                                JSONObject statusDetails = dataArray.getJSONObject(i);
                                int postId = statusDetails.getInt("id");
                                String likesCount = String.valueOf(statusDetails.getInt("likes"));
                                String commentCount = statusDetails.getString("replies");
                                String dislikesCount = String.valueOf(statusDetails.getInt("dislikes"));
                                String viewsCount = statusDetails.getString("views");
                                String statusContent = statusDetails.getString("body");

                                JSONObject timeDetails = statusDetails.getJSONObject("created");
                                String timeCreated = timeDetails.getString("date");

                                JSONObject userDetails = statusDetails.getJSONObject("user");
                                String username = userDetails.getString("name");
                                String avatarUrl = userDetails.getString("avatarUrl");


                                mStatusList.add(new Status(avatarUrl, username, timeCreated,
                                        viewsCount, statusContent, likesCount, dislikesCount, commentCount, postId));

                            }

                            mSwipeRefreshLayout.setRefreshing(false);
                            mStatusAdapter.notifyDataSetChanged();
                            mStatusAdapter.setLoaded();


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                mProgressBar.setVisibility(View.GONE);
                mCurrentPage--;
                error.printStackTrace();
            }
        });

        NetworkRequest.getInstance().addToQueue(statusRequest);

    }


}

