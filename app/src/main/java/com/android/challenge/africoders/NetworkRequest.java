package com.android.challenge.africoders;

import android.app.Application;
import android.text.TextUtils;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;


public class NetworkRequest extends Application {

    private static final String TAG = NetworkRequest.class.getSimpleName();
    private static NetworkRequest sNetworkRequest;
    private RequestQueue mRequestQueue;

    public static synchronized NetworkRequest getInstance() {
        return sNetworkRequest;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        sNetworkRequest = this;
    }

    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(getApplicationContext());
        }

        return mRequestQueue;
    }

    public <T> void addToQueue(Request<T> request, String tag) {
        request.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
        getRequestQueue().add(request);
    }

    public <T> void addToQueue(Request<T> request) {
        request.setTag(TAG);
        getRequestQueue().add(request);
    }

    public void cancelPendingRequestS(Object tag) {
        if (mRequestQueue != null) {
            mRequestQueue.cancelAll(tag);
        }
    }


}
