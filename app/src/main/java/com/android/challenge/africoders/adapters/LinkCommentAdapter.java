package com.android.challenge.africoders.adapters;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.challenge.africoders.R;
import com.android.challenge.africoders.models.LinkComment;
import com.android.challenge.africoders.utils.FormatTime;
import com.bumptech.glide.Glide;

import java.util.List;

public class LinkCommentAdapter extends RecyclerView.Adapter<LinkCommentAdapter.LinkCommentViewHolder> {
    private Context mContext;
    private List<LinkComment> mCommentList;

    public LinkCommentAdapter(Context context, List<LinkComment> commentList) {
        mContext = context;
        mCommentList = commentList;
    }

    @Override
    public LinkCommentAdapter.LinkCommentViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.link_comments_list_item, parent, false);

        return new LinkCommentViewHolder(view);
    }

    @Override
    public void onBindViewHolder(LinkCommentAdapter.LinkCommentViewHolder viewHolder, int position) {
        LinkComment linkComment = mCommentList.get(position);
        //noinspection deprecation
        String commentBody = Html.fromHtml(linkComment.getCommentBody()).toString();
        commentBody = commentBody.trim();

        Glide.with(mContext).load(linkComment.getUserAvatarUrl()).into(viewHolder.mUserAvatar);

        viewHolder.mUsername.setText(linkComment.getCommentUsername());
        viewHolder.mTimePosted.setText(FormatTime.formatTime(linkComment.getTimePosted()));
        viewHolder.mCommentBody.setText(commentBody);
        viewHolder.mCommentLikes.setText(linkComment.getCommentLikesCount());
        viewHolder.mCommentDislikes.setText(linkComment.getCommentDislikesCount());


    }

    @Override
    public int getItemCount() {
        return mCommentList.size();
    }

    class LinkCommentViewHolder extends RecyclerView.ViewHolder {
        private ImageView mUserAvatar;
        private TextView mUsername, mTimePosted, mCommentBody, mCommentLikes, mCommentDislikes;

        LinkCommentViewHolder(View itemView) {
            super(itemView);

            mUserAvatar = itemView.findViewById(R.id.link_comment_user_avatar);
            mUsername = itemView.findViewById(R.id.link_comment_username);
            mTimePosted = itemView.findViewById(R.id.link_comment_time_posted);
            mCommentBody = itemView.findViewById(R.id.link_comment_body);
            mCommentLikes = itemView.findViewById(R.id.link_comment_likes);
            mCommentDislikes = itemView.findViewById(R.id.link_comment_dislikes);
        }

    }

}
