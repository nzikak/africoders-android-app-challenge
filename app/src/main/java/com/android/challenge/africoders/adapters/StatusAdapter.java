package com.android.challenge.africoders.adapters;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.challenge.africoders.R;
import com.android.challenge.africoders.activities.EditStatusActivity;
import com.android.challenge.africoders.activities.StatusActivity;
import com.android.challenge.africoders.activities.ViewStatusActivity;
import com.android.challenge.africoders.interfaces.LoadMore;
import com.android.challenge.africoders.models.Status;
import com.android.challenge.africoders.utils.FormatTime;
import com.android.challenge.africoders.utils.PostUtils;
import com.bumptech.glide.Glide;

import java.util.List;


public class StatusAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final String LOG_TAG = StatusAdapter.class.getSimpleName();
    private static final int STATUS_VIEW_TYPE = 0, LOADING_VIEW_TYPE = 1;

    private List<Status> mStatusList;
    private Activity mActivity;
    private String mUsername, mUserAvatarUrl, mUserAuthCode;
    private boolean mIsLoading;
    private int mLastVisibleItem, mTotalItemsCount;
    private int mVisibleItemCount;
    private LoadMore mLoadMore;
    private String mStatusContent;
    private String mContent;

    public StatusAdapter(Activity activity, List<Status> statusList, RecyclerView recyclerView,
                         String username, String userAvatarUrl, String userAuthCode) {
        mStatusList = statusList;
        mActivity = activity;
        mUsername = username;
        mUserAvatarUrl = userAvatarUrl;
        mUserAuthCode = userAuthCode;

        final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                mVisibleItemCount = linearLayoutManager.getChildCount();
                mTotalItemsCount = linearLayoutManager.getItemCount();
                mLastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();

                if (!mIsLoading && mTotalItemsCount < (mLastVisibleItem + mVisibleItemCount)) {
                    if (mLoadMore != null) {
                            mLoadMore.onLoadMore();
                            mIsLoading = true;
                        }

                    }
                }
        });
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == STATUS_VIEW_TYPE) {
            View view = LayoutInflater.from(mActivity).inflate(R.layout.status_list_item, parent, false);
            return new StatusViewHolder(view);
        } else if (viewType == LOADING_VIEW_TYPE) {
            View view = LayoutInflater.from(mActivity).inflate(R.layout.loading_items_progress_bar, parent, false);

            return new LoadMoreViewHolder(view);
        }

        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        if (viewHolder instanceof StatusViewHolder) {
            final Status status = mStatusList.get(position);

            final StatusViewHolder statusViewHolder = (StatusViewHolder) viewHolder;
            //noinspection deprecation
            mStatusContent = Html.fromHtml(status.getStatusContent()).toString();
            mStatusContent = mStatusContent.trim();

            Glide.with(mActivity).load(status.getUserAvatarUrl()).into(statusViewHolder.userAvatarView);

            statusViewHolder.usernameView.setText(status.getUsername());
            statusViewHolder.statusViewsCountView.setText(status.getStatusViews() + " Views");
            statusViewHolder.statusContentView.setText(mStatusContent);
            statusViewHolder.statusLikesView.setText(status.getStatusLikes());
            statusViewHolder.statusDislikesView.setText(status.getStatusDislikes());
            statusViewHolder.statusCommentsView.setText(status.getStatusComments());
            statusViewHolder.timePostedView.setText(FormatTime.formatTime(status.getTimePosted()));

        }

        else if(viewHolder instanceof LoadMoreViewHolder) {
            LoadMoreViewHolder loadMoreViewHolder = (LoadMoreViewHolder) viewHolder;

            loadMoreViewHolder.mProgressBar.setIndeterminate(true);
        }


    }


    private class StatusViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        ImageView userAvatarView;
        TextView usernameView, timePostedView, statusViewsCountView, statusContentView, statusLikesView,
                statusDislikesView, statusCommentsView;

        ImageButton moreButton;


        StatusViewHolder(final View itemView) {
            super(itemView);

            userAvatarView = itemView.findViewById(R.id.status_user_avatar);
            usernameView = itemView.findViewById(R.id.status_username_text_view);
            timePostedView = itemView.findViewById(R.id.status_time_text_view);
            statusViewsCountView = itemView.findViewById(R.id.status_views_count);

            moreButton = itemView.findViewById(R.id.status_list_more_button);

            statusContentView = itemView.findViewById(R.id.status_content_text_view);

            statusLikesView = itemView.findViewById(R.id.status_likes_view);
            statusDislikesView = itemView.findViewById(R.id.status_dislikes_view);
            statusCommentsView = itemView.findViewById(R.id.status_comments_view);



            moreButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    //noinspection deprecation
                    mContent = Html.fromHtml(mStatusList.get(getAdapterPosition()).getStatusContent()).toString();
                    mContent = mContent.trim();
                    PopupMenu popupMenu = new PopupMenu(mActivity, moreButton, Gravity.CENTER);
                    popupMenu.getMenuInflater().inflate(R.menu.popup_menu, popupMenu.getMenu());
                    final int postId = mStatusList.get(getAdapterPosition()).getStatusId();

                    Menu popMenu = popupMenu.getMenu();
                    if(!mUsername.matches(mStatusList.get(getAdapterPosition()).getUsername())) {
                        popMenu.findItem(R.id.popup_menu_edit).setVisible(false);
                        popMenu.findItem(R.id.popup_menu_delete).setVisible(false);
                        return;
                    }

                    popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                        @Override
                        public boolean onMenuItemClick(MenuItem item) {
                            switch(item.getItemId()) {
                                case R.id.popup_menu_edit:
                                    Intent editStatusIntent = new Intent(mActivity, EditStatusActivity.class);
                                    editStatusIntent.putExtra("username_key", mUsername);
                                    editStatusIntent.putExtra("user_avatar_url_key", mUserAvatarUrl);
                                    editStatusIntent.putExtra("post_content_key", mContent);
                                    editStatusIntent.putExtra("auth_code_key", mUserAuthCode);
                                    editStatusIntent.putExtra("post_id_key", postId);
                                    editStatusIntent.putExtra("adapter_position_key", getAdapterPosition());

                                    mActivity.startActivityForResult(editStatusIntent, StatusActivity.EDIT_STATUS_ACTIVITY_REQUEST_CODE);
                                    return true;
                                case R.id.popup_menu_delete:
                                    AlertDialog.Builder builder = new AlertDialog.Builder(mActivity);
                                    builder.setCancelable(true);
                                    builder.setTitle("Delete Status?");
                                    builder.setMessage("Are you sure you want to delete status?");
                                    builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int i) {
                                            dialog.cancel();
                                        }
                                    });

                                    builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int i) {
                                            boolean isDeleted = PostUtils.isStatusDeleted(postId, mUsername, mUserAuthCode, mActivity);
                                            if(isDeleted) {
                                                mStatusList.remove(getAdapterPosition());
                                                notifyItemRemoved(getAdapterPosition());
                                                Toast.makeText(mActivity, "Status Deleted!", Toast.LENGTH_SHORT).show();
                                            }
                                        }
                                    });

                                    AlertDialog alert = builder.create();
                                    alert.show();

                                    return true;
                                default:
                                    return false;
                            }
                        }
                    });

                    popupMenu.show();
                }
            });


            itemView.setOnClickListener(this);

        }

        @Override
        public void onClick(View view) {
            Status status = mStatusList.get(getAdapterPosition());
            Intent intent = new Intent(mActivity, ViewStatusActivity.class);
            intent.putExtra("post_id", status.getStatusId());
            intent.putExtra("username", mUsername);
            intent.putExtra("user_avatar_url", mUserAvatarUrl);
            intent.putExtra("user_auth_code", mUserAuthCode);
            mActivity.startActivity(intent);
        }

    }

    private class LoadMoreViewHolder extends RecyclerView.ViewHolder {
        ProgressBar mProgressBar;

        LoadMoreViewHolder(View itemView) {
            super(itemView);

            mProgressBar = itemView.findViewById(R.id.load_more_progress_bar);
        }
    }

    @Override
    public int getItemCount() {
        return mStatusList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return mStatusList.get(position) == null ? LOADING_VIEW_TYPE : STATUS_VIEW_TYPE;
    }

    public void setLoadMore(LoadMore loadMore) {
        mLoadMore = loadMore;
    }

    public void setLoaded() {
        mIsLoading = false;
    }


}

