package com.android.challenge.africoders.activities;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.challenge.africoders.NetworkRequest;
import com.android.challenge.africoders.R;
import com.android.challenge.africoders.adapters.BlogAdapter;
import com.android.challenge.africoders.interfaces.LoadMore;
import com.android.challenge.africoders.models.Blog;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class BlogActivity extends AppCompatActivity {

    private List<Blog> mBlogList;
    private RecyclerView mRecyclerView;
    private BlogAdapter mBlogAdapter;
    private SwipeRefreshLayout mRefreshLayout;
    private FloatingActionButton mPostButton;
    private ProgressBar mProgressBar;
    private String mUsername, mUserAvatarUrl, mUserAuthCode;
    private int mCurrentPage = 1;
    private int mTotalPages;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_blog);

        Bundle userDetails = getIntent().getExtras();
        if(userDetails != null) {
            mUsername = userDetails.getString("username");
            mUserAvatarUrl = userDetails.getString("user_avatar_url");
            mUserAuthCode = userDetails.getString("user_auth_code");
        }

        mRecyclerView = findViewById(R.id.blog_activity_recycler_view);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mRefreshLayout = findViewById(R.id.blog_activity_swipeRefresh);
        mPostButton = findViewById(R.id.blog_activity_floating_action_button);
        mProgressBar = findViewById(R.id.blog_activity_progress_bar);

        getBlogItems();

        mPostButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        mRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getBlogItems();
            }
        });
    }

    private void getBlogItems() {
        String blogUrl = "https://api.africoders.com/v1/posts?category=blog&order=published_at|desc&limit=20";


        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnectedOrConnecting()) {
            JsonObjectRequest blogRequest = new JsonObjectRequest(Request.Method.GET, blogUrl, null,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            mProgressBar.setVisibility(View.GONE);
                            try {
                                if(mRefreshLayout.isRefreshing()) {
                                    mBlogList.clear();
                                    mBlogAdapter.notifyDataSetChanged();
                                }
                                else {
                                    mBlogList = new ArrayList<>();
                                }
                                JSONArray dataArray = response.getJSONArray("data");
                                for (int i = 0; i < dataArray.length(); i++) {
                                    JSONObject blogDetails = dataArray.getJSONObject(i);
                                    int postId = blogDetails.getInt("id");
                                    String likesCount = String.valueOf(blogDetails.getInt("likes"));
                                    String commentCount = blogDetails.getString("replies");
                                    String dislikesCount = String.valueOf(blogDetails.getInt("dislikes"));
                                    String viewsCount = blogDetails.getString("views");
                                    String blogTitle = blogDetails.getString("title");
                                    String blogContent = blogDetails.getString("body");

                                    JSONObject timeDetails = blogDetails.getJSONObject("created");
                                    String timeCreated = timeDetails.getString("date");

                                    JSONObject userDetails = blogDetails.getJSONObject("user");
                                    String username = userDetails.getString("name");
                                    String avatarUrl = userDetails.getString("avatarUrl");


                                    mBlogList.add(new Blog(avatarUrl, username, blogTitle, blogContent, timeCreated,
                                            viewsCount,commentCount, likesCount, dislikesCount, postId));

                                }

                                JSONObject metaObjectDetails = response.getJSONObject("meta");
                                JSONObject paginationObjectDetails = metaObjectDetails.getJSONObject("pagination");
                                mTotalPages = paginationObjectDetails.getInt("total_pages");

                                if(mRefreshLayout.isRefreshing()) {
                                    mBlogAdapter.notifyDataSetChanged();
                                    mRefreshLayout.setRefreshing(false);
                                    mCurrentPage = 1;
                                }
                                else {
                                    updateBlogList(mBlogList);
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    mProgressBar.setVisibility(View.GONE);
                    error.printStackTrace();
                }
            });

            NetworkRequest.getInstance().addToQueue(blogRequest);

        } else {
            mProgressBar.setVisibility(View.GONE);
            Toast.makeText(BlogActivity.this, "No Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }

    private void updateBlogList(List<Blog> blogList) {
        mBlogAdapter = new BlogAdapter(BlogActivity.this, blogList, mRecyclerView, mUsername, mUserAvatarUrl, mUserAuthCode);


        mRecyclerView.setAdapter(mBlogAdapter);
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnectedOrConnecting()) {
            mBlogAdapter.setLoadMore(new LoadMore() {
                @Override
                public void onLoadMore() {
                    if (mCurrentPage <= mTotalPages) {
                        mBlogList.add(null);
                        mBlogAdapter.notifyItemInserted(mBlogList.size() - 1);
                        mCurrentPage++;
                        loadMoreBlogPost(mCurrentPage);
                    } else {
                        Toast.makeText(BlogActivity.this, "No more blog posts", Toast.LENGTH_SHORT).show();
                    }
                }
            });
        } else {
            mCurrentPage--;
            Toast.makeText(BlogActivity.this, "No Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }

    private void loadMoreBlogPost(int pageOffset) {

        String statusUrl = "https://api.africoders.com/v1/posts?category=blog&order=published_at|desc&limit=20&page=" + pageOffset;

        JsonObjectRequest statusRequest = new JsonObjectRequest(Request.Method.GET, statusUrl, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            mBlogList.remove(mBlogList.size() - 1);
                            mBlogAdapter.notifyItemRemoved(mBlogList.size() - 1);

                            JSONArray dataArray = response.getJSONArray("data");
                            for (int i = 0; i < dataArray.length(); i++) {
                                JSONObject blogDetails = dataArray.getJSONObject(i);
                                int postId = blogDetails.getInt("id");
                                String likesCount = String.valueOf(blogDetails.getInt("likes"));
                                String commentCount = blogDetails.getString("replies");
                                String dislikesCount = String.valueOf(blogDetails.getInt("dislikes"));
                                String viewsCount = blogDetails.getString("views");
                                String blogTitle = blogDetails.getString("title");
                                String blogContent = blogDetails.getString("body");

                                JSONObject timeDetails = blogDetails.getJSONObject("created");
                                String timeCreated = timeDetails.getString("date");

                                JSONObject userDetails = blogDetails.getJSONObject("user");
                                String username = userDetails.getString("name");
                                String avatarUrl = userDetails.getString("avatarUrl");


                                mBlogList.add(new Blog(avatarUrl, username, blogTitle, blogContent, timeCreated,
                                        viewsCount,commentCount, likesCount, dislikesCount, postId));

                            }

                            mRefreshLayout.setRefreshing(false);
                            mBlogAdapter.notifyDataSetChanged();
                            mBlogAdapter.setLoaded();


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                mProgressBar.setVisibility(View.GONE);
                mCurrentPage--;
                error.printStackTrace();
            }
        });

        NetworkRequest.getInstance().addToQueue(statusRequest);

    }
}
