package com.android.challenge.africoders.utils;


import android.util.Log;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public class FormatTime {

    public static String formatTime(String timeString) {
        String time = null;

        String suffix = "ago";

        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date statusTime = dateFormat.parse(timeString);


            Date nowTime = new Date();

            long timeDifference = nowTime.getTime() - statusTime.getTime();

            long second = TimeUnit.MILLISECONDS.toSeconds(timeDifference);
            long minute = TimeUnit.MILLISECONDS.toMinutes(timeDifference);
            long hour = TimeUnit.MILLISECONDS.toHours(timeDifference);
            long day = TimeUnit.MILLISECONDS.toDays(timeDifference);


            if (second < 60) {
                time = "Just now";
            } else if (minute < 60) {
                time = minute + " minutes " + suffix;
            } else if (hour < 24) {
                if (hour == 1) {
                    time = hour + " hour " + suffix;
                } else {
                    time = hour + " hours " + suffix;
                }
            } else if (day < 7) {
                if (day == 1) {
                    time = "Yesterday";
                } else {
                    time = day + " days " + suffix;
                }
            } else if (day >= 7) {
                if (day < 30) {
                    if (day / 7 == 1) {
                        time = "Last week";
                    } else {
                        time = day / 7 + " weeks " + suffix;
                    }
                } else if (day < 365) {
                    if (day / 30 == 1) {
                        time = "Last month ";
                    } else {
                        time = day / 30 + " months " + suffix;
                    }
                } else {
                    if (day / 365 == 1) {
                        time = "One year " + suffix;
                    } else {
                        time = day / 365 + " years " + suffix;
                    }
                }
            }


        } catch (ParseException e) {
            e.printStackTrace();
        }

        return time;
    }
}
