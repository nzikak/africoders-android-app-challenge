This application was created as part of the [Nairaland](https://www.nairaland.com/4893863/nairaland-programming-competition-2018-third)
programming challenge.

* **Minimum Requirement**
* Android Version 4.1 Jellybean 

* **App Features**
* Registration & SignIn
* View status, blog posts, links and jobs
* Post a new status
* Comment on status, jobs, links and blog posts 
* Edit and delete a status

Download the [Africoders](https://africoders.com) app from **[Google drive](https://drive.google.com/file/d/1GPy_pclG7ZK9-_Z7rijSfWemF9azmtl1/view)**